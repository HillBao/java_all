package socket_part1;

import java.io.*;
import java.net.Socket;

public class ServeThread extends Thread {
	
	private Socket socket = null;
	private BufferedReader br = null;
	private PrintWriter pw = null;
	
	public ServeThread(Socket socket){
		this.socket = socket;
		
		try {
			br = new BufferedReader(new InputStreamReader(socket.getInputStream()));
			pw = new PrintWriter(new BufferedWriter(new OutputStreamWriter(socket.getOutputStream())),true);
			start();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public void run() {  
        while(true){  
            String str;  
            try {  
                str = br.readLine();  
                if(str.equals("END")){  
                    br.close();  
                    pw.close();  
                    socket.close();  
                    break;  
                }  
                System.out.println("Client Socket Message:"+str);  
                pw.println("Message Received");  
                pw.flush();  
            } catch (Exception e) {  
                try {  
                    br.close();  
                    pw.close();  
                    socket.close();  
                } catch (IOException e1) {  
                    // TODO Auto-generated catch block  
                    e1.printStackTrace();  
                }  
            }  
        }  
    } 
}
