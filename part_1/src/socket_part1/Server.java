package socket_part1;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;

/**
 * @author mac
 *
 */
public class Server {
	public static int PORT = 8080;
	public static void main(String[] args) {
		ServerSocket serverSocket = null;
		Socket socket = null;
		BufferedReader br = null;
		PrintWriter pw = null;
		
		try {
			//---------------设定服务器端口号
			serverSocket = new ServerSocket(PORT);
			System.out.println("ServerSocket Start:"+serverSocket);
			//---------------等待请求，此方法会一直阻塞，直到获得请求才往下走
			socket = serverSocket.accept();
			System.out.println("Connection accept socket:"+socket);
			//建立读接口，用于接收客户端发来的信息
			br = new BufferedReader(new InputStreamReader(socket.getInputStream()));
			//建立写接口，用于写数据
			pw = new PrintWriter(new BufferedWriter(new OutputStreamWriter(socket.getOutputStream())),true);
			while(true){
				String str = br.readLine();
				if(str.equals("END")){
					break;
				}
				System.out.println("Client Socket Message:"+str);
				try {
					Thread.sleep(1000);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				pw.println("Message Received");
				pw.flush();
			}
			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally{
			System.out.println("Close...");
			
			
			try {
				pw.close();
				br.close();
				socket.close();
				serverSocket.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
		}
	}
	
}
