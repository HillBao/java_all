package socket_part1;

import java.io.*;
import java.net.*;


public class Client {
	public static void main(String[] args) {
		Socket socket = null;
		BufferedReader br = null;
		PrintWriter pw = null;
		
		try {
			socket = new Socket("127.0.0.1",Server.PORT);
			System.out.println("Socket="+socket);
			//同服务器原理一样
			br = new BufferedReader(new InputStreamReader(socket.getInputStream()));
			pw = new PrintWriter(new BufferedWriter(new OutputStreamWriter(socket.getOutputStream())));
			for (int i = 0; i < 10; i++) {
				pw.println("howdy" +i);
				pw.flush();
				String str = br.readLine();
				System.out.println(str);
			}
			pw.println("END");
			pw.flush();
		} catch (UnknownHostException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
