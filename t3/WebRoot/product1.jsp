﻿<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8" import="java.util.*"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<!DOCTYPE html>
<!--[if IE 7]>                  <html class="ie7 no-js" lang="en">     <![endif]-->
<!--[if lte IE 8]>              <html class="ie8 no-js" lang="en">     <![endif]-->
<!--[if (gte IE 9)|!(IE)]><!--> <html class="not-ie no-js" lang="en">  <!--<![endif]-->
<head>

	<!-- Basic Page Needs
  ================================================== -->
	<meta charset="utf-8" />
	<title>核心技术</title>
	<meta name="description" content="" />
	<meta name="author" content="" />
	
	<!-- Mobile Specific Metas
  ================================================== -->
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
	
	
	<!-- CSS
  ================================================== -->
	<!-- Normalize default styles -->
	<link rel="stylesheet" href="css/normalize.css" media="screen" />
	<!-- Fonts -->
	<link rel="stylesheet" href="css/fonts.css" media="screen" />
	<!-- Skeleton grid system -->
	<link rel="stylesheet" href="css/skeleton.css" media="screen" />
	<!-- Base Template Styles-->
	<link rel="stylesheet" href="css/base.css" media="screen" />
	<!-- Superfish Menu-->
	<link rel="stylesheet" href="css/superfish.css" media="screen" />
	<!-- Template Styles-->
	<link rel="stylesheet" href="css/style.css" media="screen" />
	<!-- PrettyPhoto -->
	<link rel="stylesheet" href="css/prettyPhoto.css" media="screen" />
	<!-- Flexslider -->
	<link rel="stylesheet" href="css/flexslider.css" media="screen" />
	<!-- Layout and Media queries-->
	<link rel="stylesheet" href="css/layout.css" media="screen" />
	<!--[if lt IE 9]>
		<link rel="stylesheet" href="css/ie/ie8.css" media="screen" />
	<![endif]-->
	
	
	<!--[if lt IE 9]>
		<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
	<![endif]-->
	
	
	<!-- Favicons
	================================================== -->
	<link rel="shortcut icon" href="images/favicon.ico" />
	<link rel="apple-touch-icon" href="images/apple-touch-icon.png" />
	<link rel="apple-touch-icon" sizes="72x72" href="images/apple-touch-icon-72x72.png" />
	<link rel="apple-touch-icon" sizes="114x114" href="images/apple-touch-icon-114x114.png" />
	
	
	<!-- For Old Browsers
	================================================== -->
	<!--[if lt IE 8]>
	<div style=' clear: both; text-align:center; position: relative;'>
		<a href="http://www.microsoft.com/windows/internet-explorer/default.aspx?ocid=ie6_countdown_bannercode"><img src="http://storage.ie6countdown.com/assets/100/images/banners/warning_bar_0000_us.jpg" border="0" alt="" /></a>
	</div>
	<![endif]-->
	
	
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" /></head>
<body class="page services-page">

	<!-- Primary Page Layout
	================================================== -->
	
	<!-- BEGIN WRAPPER -->
	<div id="wrapper">
			
			
			<!-- BEGIN HEADER -->
			<header id="header">
				
				<!--Top Header-->
				<div id="top-header">
					<div class="container">
						<div class="grid_12">
							
							<!-- Begin Language Switcher -->
							
							<ul class="lang-switcher sf-menu">
								<li><a href="#">欢迎使用</a></li>
							</ul>
							
							<!-- End Language Switcher -->
							
							<div class="login-holder fright">
								<a href="#" class="signin" data-reveal-id="signin">登录</a><a href="#" class="signin" data-reveal-id="signup">注册</a>
							</div>
						</div>
					</div>
				</div>
				
				<!--Main Header-->
				<div id="main-header">
					<div class="container">
						<div class="grid_12">
						
							<!-- BEGIN LOGO -->
							<div id="logo">
								<!-- Image based Logo-->
								<a href="index.jsp"><img src="images/logo.png" alt="" /></a>
								
								
								<!-- Text based Logo
								<h1><a href="index.html"><span>Optima</span>Sales</a></h1>
								<p class="tagline">optimize your business</p>
								-->
								
							</div>
							<!-- END LOGO -->
							
							<!-- BEGIN NAVIGATION -->
							<nav class="primary">
								<ul class="sf-menu">
									<li class="current-menu-item"><a href="index.jsp">首&nbsp;&nbsp;&nbsp;&nbsp;页</a></li>
									<li><a href="#">产品介绍</a>
										<ul>
											<li><a href="product.jsp">产品特征</a></li>
											<li><a href="product1.jsp">核心技术</a></li>
											<li><a href="product2.jsp">产品研发</a></li>
											<li><a href="product3.jsp">市场分析</a></li>
										</ul>
									</li>
									<li><a href="company.jsp">公司概况</a>
									</li>
									<li><a href="about-us.jsp">关于我们</a>
									<ul>
											<li><a href="contact-us.jsp">联系我们</a></li>
									</ul>
									</li>
									<li><a href="#">进入后台</a></li>
									
								</ul>
							</nav>
							<!-- END NAVIGATION -->
							
						</div>
					</div>
				</div>
				
			</header>
			<!-- END HEADER -->
			
			
			
			<!-- BEGIN PAGE HEADING -->
			<section id="heading">
				
				<div class="container">
						
					<div class="grid_8">
						<!-- BEGIN PAGE HEADING -->
						<header class="page-heading">
							<h1>核心技术</h1>
							<nav class="breadcrumbs">
								<ul>
									<li><a href="index.jsp">首页</a></li>
									<li><a href="product.jsp">产品特征</a></li>
									<li class="current">核心技术</li>
								</ul>
							</nav>
						</header>
						<!-- END PAGE HEADING -->
					</div>
					
					<div class="grid_4">
						<div class="prefix_1_2">
							<!-- BEGIN SEARCH FORM -->
							<form id="search-form" class="search-form" action="#" />
								<input type="text" placeholder="搜索..." />
								<input type="submit" value="" />
							</form>
							<!-- END SEARCH FORM -->
						</div>
					</div>
						
				</div>
				
			</section>
			<!-- END PAGE HEADING -->
			
			
			
			<!-- BEGIN CONTENT HOLDER -->
			<div id="content-wrapper">
				
				<section class="indent">
					
					<div class="container">
						
						<div id="content" class="grid_8">
							
							<h2>数据挖掘及其意义</h2>
							<p>&nbsp;&nbsp;&nbsp;&nbsp;我们公司初期主要产品为附以人工智能肿瘤辅助诊断系统的6项肿瘤标志物（CEA 、Gastrin 、NSE 、SA 、Cu／Zn比值、Ca）联合检测试剂盒。我们的核心技术为:①肿瘤标志物联合检测应用于肿瘤的早期筛查;②能够明显提高肿瘤筛查准确度和特异度的人工智能肿瘤辅助诊断系统。在此将对我们的核心技术进行详细说明。
肿瘤标志物（Tumor marker，TM）是指在肿瘤发生和增生过程中，由肿瘤细胞生物合成、释放或者是肿瘤与宿主相互作用而产生的一类物质，它们或不存在于正常成人组织而仅见于胚胎组织，或在肿瘤组织中的含量大大超过在正常组织里的含量，它们的存在或量变可以提示肿瘤的性质，借以了解肿瘤的组织发生、细胞分化、细胞功能，以帮助肿瘤的诊断、分类、预后判断以及治疗指导。
本公司对血清中12项肿瘤标志物进行联合检测，发现CEA、Gastrin、NSE、SA、Cu/Zn比值、Ca 这6项指标对肺癌的诊断价值更大，从而筛选出了诊断肺癌的“最优肿瘤标志群”。我们还进一步研究这6项肿瘤标志联合诊断食管癌、胃癌、肠癌和肝癌中的作用，发现诊断这些肿瘤的灵敏度和特异度均令人满意。
人工智能算法很多，我们系统初期采用的为人工神经网络和决策树算法。人工神经网络（Artificial Neural Network，ANN）是模拟生物大脑神经网络的结构和功能的一种信息处理系统，其实质是一种反映输入转化为输出的数学表达式，通过网络结构中的数学公式来模拟大脑的学习功能，将学习到的规律存储到网络结构中，并通过记忆和联想实现对信息的智能化分析和预测，是近年来在各领域研究中较为流行的一种模型。决策树算法(Decision Tree）是一种逼近离散函数值的方法。它是一种典型的分类方法，首先对数据进行处理，利用归纳算法生成可读的规则和决策树，然后使用决策对新数据进行分析。本质上决策树是通过一系列规则对数据进行分类的过程。艾特诊断系统依据不同的数据库选用准确度最高的算法。
使用者将肿瘤标志物检测结果输入已经建立好的人工智能系统中，两者联合进行诊断，可以最大限度地为受检者提供准确详实的健康信息。
</p>  
							
							
							<div class="spacer"></div><!--//.spacer -->
						
							  
							 
												
							
						</div>
						
						<!-- BEGIN SIDEBAR -->
						<aside id="sidebar" class="grid_4">
							
							<div class="prefix_1_2">
								
								<!-- BEGIN CUSTOM MENU WIDGET -->
								<div class="widget custom-menu-widget">
									<h4>核心技术:</h4>
									<ul>
										<li><a href="product.jsp">产品特征.</a></li>
										<li><a href="product1.jsp">核心技术.</a></li>
										<li><a href="product2.jsp">产品研发.</a></li>
										<li><a href="product3.jsp">市场分析.</a></li>
									</ul> 
							  </div>
								<!-- END WIDGET -->
								
								<!-- BEGIN CUSTOM MENU WIDGET -->
								<!-- END WIDGET -->
                            </div>
						</aside>
						<!-- END SIDEBAR -->
						
					</div>
					
				</section>
				
			</div>
			<!-- END CONTENT HOLDER -->
			
			
			<!-- BEGIN WIDGETS --><!-- END WIDGETS -->
			
			
			<!-- BEGIN FOOTER -->
			<footer id="footer">
				<div class="container" align="center">
					<div class="grid_12">
						<small>版权所有</small><a href="http://www.cssmoban.com/" title="网站模板" target="_blank"></a>
					</div>
				</div>
			</footer>
			<!-- END FOOTER -->
			
		
	</div>
	<!-- END WRAPPER -->
	
	
	<!-- BEGIN MODAL WINDOWS -->
	<div id="signin" class="reveal-modal">
		<header class="reveal-modal-header">
			用户登录
		</header>
		<div class="cont">
			<div class="indication">
				<em id="e" style="color:red"/></em>
			</div>
			<form action="loginAction" class="signin" />
				<p class="clearfix">
					<label>邮箱:</label>
					<input type="text" name="user.uemail" id="email"/>
				</p>
				<p class="clearfix">
					<label>密码:</label>
					<input type="text" name="user.upassword" id="password"/>
				</p>
				<p class="checkboxes without-label clearfix">
					<a href="#">忘记密码?</a>
				</p>
				<p class="without-label clearfix"><input type="submit" value="" onClick="return loginCheck();"/></p>
			</form>
		</div>
		<a class="close-reveal-modal">&#215;</a>
	</div>

	<div id="signup" class="reveal-modal">
		<header class="reveal-modal-header">
			用户注册
		</header>
		
		<div class="cont">
			<form  action="registAction.action" method="post" class="signup"/>
				<p class="clearfix">
					<em id="error" style="color:red">(<abbr>*</abbr>)要求全部的字段</em>
					
				</p>
				<p class="clearfix">
					<label>邮箱:<abbr>*</abbr></label>
					<input type="text" name="user.uemail" id="ue"/>
				</p>
				<p class="clearfix">
					<label>昵称:<abbr>*</abbr></label>
					<input type="text"  name="user.uname" id="name"/>
				</p>
				<p class="clearfix">
					<label>密码:<abbr>*</abbr></label>
					<input type="text"  name="user.upassword" id="pwd"/>
				</p>
				<p class="clearfix">
					<label>重复密码:<abbr>*</abbr></label>
					<input type="text" name="rePassword" id="repwd"/>
				</p>
				
				<p class="checkboxes clearfix">
					<span class="niceCheck"><input type="checkbox" name="ch1" id="chh1"/></span>
					同意(阅读)条款
				</p>
				
				<p class="without-label clearfix"><input type="submit"  value="" onClick="return check();"/></p>
			
			</form>
		</div>
		<a class="close-reveal-modal">&#215;</a>
	</div>
	<!-- END MODAL WINDOWS -->
	
	
	<!-- Javascript Files
	================================================== -->
	
	<!-- initialize jQuery Library -->
	<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.1/jquery.min.js"></script>
	<script>window.jQuery || document.write('<script src="js/jquery-1.8.1.min.js"><\/script>')</script>
	<!-- Modernizr -->
	<script type="text/javascript" src="js/modernizr.custom.14583.js"></script>
	<!-- Superfish Menu -->
	<script type="text/javascript" src="js/superfish.js"></script>
	<!-- easing plugin -->
	<script type="text/javascript" src="js/jquery.easing.min.js"></script>
	<!-- Prettyphoto -->
	<script type="text/javascript" src="js/jquery.prettyPhoto.js"></script>
	<!-- Mobile Menu -->
	<script type="text/javascript" src="js/jquery.mobilemenu.js"></script>
	<!-- Twitter -->
	<script type="text/javascript" src="js/jquery.twitter.js"></script>
	<!-- Elastslide -->
	<script type="text/javascript" src="js/jquery.elastislide.js"></script>
	<!-- Custom Checkbox -->
	<script type="text/javascript" src="js/jquery.checkbox.js"></script>
	<!-- Flexslider -->
	<script type="text/javascript" src="js/jquery.flexslider.js"></script>
	<!-- Reveal Modal -->
	<script type="text/javascript" src="js/jquery.reveal.js"></script>
	<!--check login and regist -->
	<script type="text/javascript" src="js/login.js" charset="utf-8"></script>
	<!-- Custom -->
	<script type="text/javascript" src="js/custom.js"></script>
	
	<script type="text/javascript">

		var _gaq = _gaq || [];
		_gaq.push(['_setAccount', 'UA-34673973-1']);
		_gaq.push(['_trackPageview']);

		(function() {
			var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
			ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
			var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
		})();

	</script>
	
</body>
</html>