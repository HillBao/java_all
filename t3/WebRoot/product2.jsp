﻿<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8" import="java.util.*"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<!DOCTYPE html>
<!--[if IE 7]>                  <html class="ie7 no-js" lang="en">     <![endif]-->
<!--[if lte IE 8]>              <html class="ie8 no-js" lang="en">     <![endif]-->
<!--[if (gte IE 9)|!(IE)]><!--> <html class="not-ie no-js" lang="en">  <!--<![endif]-->
<head>

	<!-- Basic Page Needs
  ================================================== -->
	<meta charset="utf-8" />
	<title>产品研发</title>
	<meta name="description" content="" />
	<meta name="author" content="" />
	
	<!-- Mobile Specific Metas
  ================================================== -->
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
	
	
	<!-- CSS
  ================================================== -->
	<!-- Normalize default styles -->
	<link rel="stylesheet" href="css/normalize.css" media="screen" />
	<!-- Fonts -->
	<link rel="stylesheet" href="css/fonts.css" media="screen" />
	<!-- Skeleton grid system -->
	<link rel="stylesheet" href="css/skeleton.css" media="screen" />
	<!-- Base Template Styles-->
	<link rel="stylesheet" href="css/base.css" media="screen" />
	<!-- Superfish Menu-->
	<link rel="stylesheet" href="css/superfish.css" media="screen" />
	<!-- Template Styles-->
	<link rel="stylesheet" href="css/style.css" media="screen" />
	<!-- PrettyPhoto -->
	<link rel="stylesheet" href="css/prettyPhoto.css" media="screen" />
	<!-- Flexslider -->
	<link rel="stylesheet" href="css/flexslider.css" media="screen" />
	<!-- Layout and Media queries-->
	<link rel="stylesheet" href="css/layout.css" media="screen" />
	<!--[if lt IE 9]>
		<link rel="stylesheet" href="css/ie/ie8.css" media="screen" />
	<![endif]-->
	
	
	<!--[if lt IE 9]>
		<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
	<![endif]-->
	
	
	<!-- Favicons
	================================================== -->
	<link rel="shortcut icon" href="images/favicon.ico" />
	<link rel="apple-touch-icon" href="images/apple-touch-icon.png" />
	<link rel="apple-touch-icon" sizes="72x72" href="images/apple-touch-icon-72x72.png" />
	<link rel="apple-touch-icon" sizes="114x114" href="images/apple-touch-icon-114x114.png" />
	
	
	<!-- For Old Browsers
	================================================== -->
	<!--[if lt IE 8]>
	<div style=' clear: both; text-align:center; position: relative;'>
		<a href="http://www.microsoft.com/windows/internet-explorer/default.aspx?ocid=ie6_countdown_bannercode"><img src="http://storage.ie6countdown.com/assets/100/images/banners/warning_bar_0000_us.jpg" border="0" alt="" /></a>
	</div>
	<![endif]-->
	
	
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" /></head>
<body class="page services-page">

	<!-- Primary Page Layout
	================================================== -->
	
	<!-- BEGIN WRAPPER -->
	<div id="wrapper">
			
			
			<!-- BEGIN HEADER -->
			<header id="header">
				
				<!--Top Header-->
				<div id="top-header">
					<div class="container">
						<div class="grid_12">
							
							<!-- Begin Language Switcher -->
							
							<ul class="lang-switcher sf-menu">
								<li><a href="#">欢迎使用</a></li>
							</ul>
							
							<!-- End Language Switcher -->
							
							<div class="login-holder fright">
								<a href="#" class="signin" data-reveal-id="signin">登录</a><a href="#" class="signin" data-reveal-id="signup">注册</a>
							</div>
						</div>
					</div>
				</div>
				
				<!--Main Header-->
				<div id="main-header">
					<div class="container">
						<div class="grid_12">
						
							<!-- BEGIN LOGO -->
							<div id="logo">
								<!-- Image based Logo-->
								<a href="index.jsp"><img src="images/logo.png" alt="" /></a>
								
								
								<!-- Text based Logo
								<h1><a href="index.html"><span>Optima</span>Sales</a></h1>
								<p class="tagline">optimize your business</p>
								-->
								
							</div>
							<!-- END LOGO -->
							
							<!-- BEGIN NAVIGATION -->
							<nav class="primary">
								<ul class="sf-menu">
									<li class="current-menu-item"><a href="index.jsp">首&nbsp;&nbsp;&nbsp;&nbsp;页</a></li>
									<li><a href="#">产品介绍</a>
										<ul>
											<li><a href="product.jsp">产品特征</a></li>
											<li><a href="product1.jsp">核心技术</a></li>
											<li><a href="product2.jsp">产品研发</a></li>
											<li><a href="product3.jsp">市场分析</a></li>
										</ul>
									</li>
									<li><a href="company.jsp">公司概况</a>
									</li>
									<li><a href="about-us.jsp">关于我们</a>
									<ul>
											<li><a href="contact-us.jsp">联系我们</a></li>
									</ul>
									</li>
									<li><a href="#">进入后台</a></li>
									
								</ul>
							</nav>
							<!-- END NAVIGATION -->
							
						</div>
					</div>
				</div>
				
			</header>
			<!-- END HEADER -->
			
			
			
			<!-- BEGIN PAGE HEADING -->
			<section id="heading">
				
				<div class="container">
						
					<div class="grid_8">
						<!-- BEGIN PAGE HEADING -->
						<header class="page-heading">
							<h1>产品研发</h1>
							<nav class="breadcrumbs">
								<ul>
									<li><a href="index.jsp">首页</a></li>
									<li><a href="product.html">产品特征</a></li>
									<li class="current">产品研发</li>
								</ul>
							</nav>
						</header>
						<!-- END PAGE HEADING -->
					</div>
					
					<div class="grid_4">
						<div class="prefix_1_2">
							<!-- BEGIN SEARCH FORM -->
							<form id="search-form" class="search-form" action="#" />
								<input type="text" placeholder="搜索..." />
								<input type="submit" value="" />
							</form>
							<!-- END SEARCH FORM -->
						</div>
					</div>
						
				</div>
				
			</section>
			<!-- END PAGE HEADING -->
			
			
			
			<!-- BEGIN CONTENT HOLDER -->
			<div id="content-wrapper">
				
				<section class="indent">
					
					<div class="container">
						
						<div id="content" class="grid_8">
							
							<p><strong>把握机遇</strong><br/>
							&nbsp;&nbsp;&nbsp;&nbsp;从产品的生命周期来讲，任何一个产品都会经历成长、成熟的过程。从市场现状看，国内市场与国际市场融为一体，新的产品和技术层出不穷，产品的生命周期越来越短，我们公司要应对这种情况，必须在公司的这个发展过程中不断研发新产品，树立我们公司的形象，提高我们公司在市场中的竞争优势和竞争地位。  </p>
							
							<div class="spacer"></div><!--//.spacer -->
							<p><strong>研发模式</strong><br/>
							
							&nbsp;&nbsp;&nbsp;&nbsp;我们公司采用肿瘤标志物检测试剂盒和配套的辅助诊断系统同时研发相结合的综合模式。同时在数据库的不断扩充、新肿瘤标志物研发的基础上的，研发新产品、开发新软件。模式图如下:</p>  
							
							<!-- BEGIN LEFT ALIGN IMAGE -->
							<figure class="img-half-width alignleft">
								<img src="images/t2.png" class="fullwidth" alt="" width="301" height="381" border="0" />
							</figure>
							<p><strong>研发方式</strong><br/>
							&nbsp;&nbsp;&nbsp;&nbsp;产品的研发方式主要是肿瘤标志物检测试剂盒和肿瘤筛查与辅助诊断软件同时研发相结合的模式。
 	不断研发新的肺癌标志物指标检测试剂盒，提高肺癌诊断的灵敏度与特异度，与最新的肿瘤检测指标相适应；
 	数据库的更新要与检测人数的增加、市场的扩大相结合；
 	肿瘤预测模型的建立和软件的设计则要与数据库保持一致，不断调整模型和软件的可靠性和实用性。
 	增加新的常见肿瘤筛查种类，如胃癌、肝癌、食管癌等，研发新的肿瘤标志物检测试剂盒和肿瘤筛查软件，扩大肿瘤筛查的范围；
 	研究设计“肿瘤筛查仪”，将肿瘤标志物的检测技术和肿瘤筛查软件相结合，进一步提高工作效率。
</p>
							
							<!-- END LEFT ALIGN IMAGE -->
							
							<div class="spacer"></div><!--//.spacer -->
							
							<!-- BEGIN RIGHT ALIGN IMAGE -->
							
							<!-- END RIGHT ALIGN IMAGE -->
							<!--//.spacer -->
                            <!-- BEGIN LEFT ALIGN IMAGE -->
<p>&nbsp;</p>  
							
							<!-- END LEFT ALIGN IMAGE -->
							
							
						</div>
						
						<!-- BEGIN SIDEBAR -->
						<aside id="sidebar" class="grid_4">
							
							<div class="prefix_1_2">
								
								<!-- BEGIN CUSTOM MENU WIDGET -->
								<div class="widget custom-menu-widget">
									<h4>产品研发:</h4>
									<ul>
										<li><a href="product.jsp">产品特征.</a></li>
										<li><a href="product1.jsp">核心技术.</a></li>
										<li><a href="product2.jsp">产品研发.</a></li>
										<li><a href="product3.jsp">市场分析.</a></li>
									</ul>
							  </div>
								<!-- END WIDGET -->
								
								<!-- BEGIN CUSTOM MENU WIDGET -->
								<!-- END WIDGET -->
                            </div>
						</aside>
						<!-- END SIDEBAR -->
						
					</div>
					
				</section>
				
			</div>
			<!-- END CONTENT HOLDER -->
			
			
			<!-- BEGIN WIDGETS --><!-- END WIDGETS -->
			
			
			<!-- BEGIN FOOTER -->
			<footer id="footer">
				<div class="container" align="center">
					<div class="grid_12">
						<small>版权所有</small><a href="http://www.cssmoban.com/" title="网站模板" target="_blank"></a>
					</div>
				</div>
			</footer>
			<!-- END FOOTER -->
			
		
	</div>
	<!-- END WRAPPER -->
	
	
	<!-- BEGIN MODAL WINDOWS -->
	<div id="signin" class="reveal-modal">
		<header class="reveal-modal-header">
			用户登录
		</header>
		<div class="cont">
			<div class="indication">
				<em id="e" style="color:red"/></em>
			</div>
			<form action="loginAction" class="signin" />
				<p class="clearfix">
					<label>邮箱:</label>
					<input type="text" name="user.uemail" id="email"/>
				</p>
				<p class="clearfix">
					<label>密码:</label>
					<input type="text" name="user.upassword" id="password"/>
				</p>
				<p class="checkboxes without-label clearfix">
					<a href="#">忘记密码?</a>
				</p>
				<p class="without-label clearfix"><input type="submit" value="" onClick="return loginCheck();"/></p>
			</form>
		</div>
		<a class="close-reveal-modal">&#215;</a>
	</div>

	<div id="signup" class="reveal-modal">
		<header class="reveal-modal-header">
			用户注册
		</header>
		
		<div class="cont">
			<form  action="registAction.action" method="post" class="signup"/>
				<p class="clearfix">
					<em id="error" style="color:red">(<abbr>*</abbr>)要求全部的字段</em>
					
				</p>
				<p class="clearfix">
					<label>邮箱:<abbr>*</abbr></label>
					<input type="text" name="user.uemail" id="ue"/>
				</p>
				<p class="clearfix">
					<label>昵称:<abbr>*</abbr></label>
					<input type="text"  name="user.uname" id="name"/>
				</p>
				<p class="clearfix">
					<label>密码:<abbr>*</abbr></label>
					<input type="text"  name="user.upassword" id="pwd"/>
				</p>
				<p class="clearfix">
					<label>重复密码:<abbr>*</abbr></label>
					<input type="text" name="rePassword" id="repwd"/>
				</p>
				
				<p class="checkboxes clearfix">
					<span class="niceCheck"><input type="checkbox" name="ch1" id="chh1"/></span>
					同意(阅读)条款
				</p>
				
				<p class="without-label clearfix"><input type="submit"  value="" onClick="return check();"/></p>
			
			</form>
		</div>
		<a class="close-reveal-modal">&#215;</a>
	</div>
	<!-- END MODAL WINDOWS -->
	
	
	<!-- Javascript Files
	================================================== -->
	
	<!-- initialize jQuery Library -->
	<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.1/jquery.min.js"></script>
	<script>window.jQuery || document.write('<script src="js/jquery-1.8.1.min.js"><\/script>')</script>
	<!-- Modernizr -->
	<script type="text/javascript" src="js/modernizr.custom.14583.js"></script>
	<!-- Superfish Menu -->
	<script type="text/javascript" src="js/superfish.js"></script>
	<!-- easing plugin -->
	<script type="text/javascript" src="js/jquery.easing.min.js"></script>
	<!-- Prettyphoto -->
	<script type="text/javascript" src="js/jquery.prettyPhoto.js"></script>
	<!-- Mobile Menu -->
	<script type="text/javascript" src="js/jquery.mobilemenu.js"></script>
	<!-- Twitter -->
	<script type="text/javascript" src="js/jquery.twitter.js"></script>
	<!-- Elastslide -->
	<script type="text/javascript" src="js/jquery.elastislide.js"></script>
	<!-- Custom Checkbox -->
	<script type="text/javascript" src="js/jquery.checkbox.js"></script>
	<!-- Flexslider -->
	<script type="text/javascript" src="js/jquery.flexslider.js"></script>
	<!-- Reveal Modal -->
	<script type="text/javascript" src="js/jquery.reveal.js"></script>
	<!--check login and regist -->
	<script type="text/javascript" src="js/login.js" charset="utf-8"></script>
	<!-- Custom -->
	<script type="text/javascript" src="js/custom.js"></script>
	
	<script type="text/javascript">

		var _gaq = _gaq || [];
		_gaq.push(['_setAccount', 'UA-34673973-1']);
		_gaq.push(['_trackPageview']);

		(function() {
			var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
			ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
			var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
		})();

	</script>
	
</body>
</html>