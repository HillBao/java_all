﻿<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8" import="java.util.*"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<!DOCTYPE html>
<!--[if IE 7]>                  <html class="ie7 no-js" lang="en">     <![endif]-->
<!--[if lte IE 8]>              <html class="ie8 no-js" lang="en">     <![endif]-->
<!--[if (gte IE 9)|!(IE)]><!--> <html class="not-ie no-js" lang="en">  <!--<![endif]-->
<head>

	<!-- Basic Page Needs
  ================================================== -->
	<meta charset="utf-8" />
	<title>公司概况</title>
	<meta name="description" content="" />
	<meta name="author" content="" />
	
	<!-- Mobile Specific Metas
  ================================================== -->
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
	
	
	<!-- CSS
  ================================================== -->
	<!-- Normalize default styles -->
	<link rel="stylesheet" href="css/normalize.css" media="screen" />
	<!-- Fonts -->
	<link rel="stylesheet" href="css/fonts.css" media="screen" />
	<!-- Skeleton grid system -->
	<link rel="stylesheet" href="css/skeleton.css" media="screen" />
	<!-- Base Template Styles-->
	<link rel="stylesheet" href="css/base.css" media="screen" />
	<!-- Superfish Menu-->
	<link rel="stylesheet" href="css/superfish.css" media="screen" />
	<!-- Template Styles-->
	<link rel="stylesheet" href="css/style.css" media="screen" />
	<!-- PrettyPhoto -->
	<link rel="stylesheet" href="css/prettyPhoto.css" media="screen" />
	<!-- Flexslider -->
	<link rel="stylesheet" href="css/flexslider.css" media="screen" />
	<!-- Layout and Media queries-->
	<link rel="stylesheet" href="css/layout.css" media="screen" />
	<!--[if lt IE 9]>
		<link rel="stylesheet" href="css/ie/ie8.css" media="screen" />
	<![endif]-->
	
	
	<!--[if lt IE 9]>
		<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
	<![endif]-->
	
	
	<!-- Favicons
	================================================== -->
	<link rel="shortcut icon" href="images/favicon.ico" />
	<link rel="apple-touch-icon" href="images/apple-touch-icon.png" />
	<link rel="apple-touch-icon" sizes="72x72" href="images/apple-touch-icon-72x72.png" />
	<link rel="apple-touch-icon" sizes="114x114" href="images/apple-touch-icon-114x114.png" />
	
	
	<!-- For Old Browsers
	================================================== -->
	<!--[if lt IE 8]>
	<div style=' clear: both; text-align:center; position: relative;'>
		<a href="http://www.microsoft.com/windows/internet-explorer/default.aspx?ocid=ie6_countdown_bannercode"><img src="http://storage.ie6countdown.com/assets/100/images/banners/warning_bar_0000_us.jpg" border="0" alt="" /></a>
	</div>
	<![endif]-->
	
	
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" /></head>
<body class="page how-it-works-page">

	<!-- Primary Page Layout
	================================================== -->
	
	<!-- BEGIN WRAPPER -->
	<div id="wrapper">
			
			
			<!-- BEGIN HEADER -->
			<header id="header">
				
				<!--Top Header-->
				<div id="top-header">
					<div class="container">
						<div class="grid_12">
							
							<!-- Begin Language Switcher -->
								欢迎使用
							<!-- End Language Switcher 当前用户：-->

							<div class="login-holder fright">
							<% 
								Object sv=session.getAttribute("uname"); 
								if(sv!=null){
								
								%> 用户名：<font color="red">${sessionScope.uname}</font><a href=logout.jsp>退出</a>
								<%
								}
								else{
								 %>
								<a href="#" class="signin" data-reveal-id="signin">登录</a>
								<a href="#" class="signin" data-reveal-id="signup">注册</a>
								<%} %>
							</div>
						</div>
					</div>
				</div>
				
				<!--Main Header-->
				<div id="main-header">
					<div class="container">
						<div class="grid_12">
						
							<!-- BEGIN LOGO -->
							<div id="logo">
								<!-- Image based Logo-->
								<a href="index.jsp"><img src="images/logo.png" alt="" /></a>
								
								
								<!-- Text based Logo
								<h1><a href="index.html"><span>Optima</span>Sales</a></h1>
								<p class="tagline">optimize your business</p>
								-->
								
							</div>
							<!-- END LOGO -->
							
							<!-- BEGIN NAVIGATION -->
							<nav class="primary">
								<ul class="sf-menu">
									<li class="current-menu-item"><a href="index.jsp">首&nbsp;&nbsp;&nbsp;&nbsp;页</a></li>
									<li><a href="#">产品介绍</a>
										<ul>
											<li><a href="product.jsp">产品特征</a></li>
											<li><a href="product1.jsp">核心技术</a></li>
											<li><a href="product2.jsp">产品研发</a></li>
											<li><a href="product3.jsp">市场分析</a></li>
										</ul>
									</li>
									<li><a href="company.jsp">公司概况</a>
									</li>
									<li><a href="about-us.jsp">关于我们</a>
									<ul>
											<li><a href="contact-us.jsp">联系我们</a></li>
									</ul>
									</li>
									<li><a href="#">进入后台</a></li>
									
								</ul>
							</nav>
							<!-- END NAVIGATION -->
							
						</div>
					</div>
				</div>
				
			</header>
			<!-- END HEADER -->
			
			
			
			<!-- BEGIN VIDEO PRESENTATION -->
			<section id="heading">
              <div class="container">
                <div class="grid_8">
                  <!-- BEGIN PAGE HEADING -->
                  <header class="page-heading">
                    <h1>公司概况</h1>
                    <nav class="breadcrumbs">
                      <ul>
                        <li><a href="index.html">首页</a></li>
                        <li class="current">联系我们</li>
                      </ul>
                    </nav>
                  </header>
                  <!-- END PAGE HEADING -->
                </div>
                <div class="grid_4">
                  <div class="prefix_1_2">
                    <!-- BEGIN SEARCH FORM -->
                    <form id="search-form" class="search-form" action="#" />
                    <input name="text" type="text" placeholder="搜索..." />
                    <input name="submit" type="submit" value="" />
                    </form>
                    <!-- END SEARCH FORM -->
                  </div>
                </div>
              </div>
	  </section>
			<!-- END VIDEO PRESENTATION -->
			
			
			
			<!-- BEGIN CONTENT HOLDER -->
			<div id="content-wrapper">
				
				<section class="indent">
					
					<div class="container">
						<div class="grid_12">
							
							<!-- BEGIN TABBED CONTENT -->
							<div class="tabs full-w">
						
								<div class="tab-menu">
									<ul>
										<li><a href="#tab1">公司简介<i class="l-tab-shad"></i><i class="r-tab-shad"></i></a></li>
										<li><a href="#tab2">企业理念<i class="l-tab-shad"></i><i class="r-tab-shad"></i></a></li>
										<li><a href="#tab3">品牌理念</a></li>
										<li><a href="#tab4">商标寓意<i class="l-tab-shad"></i><i class="r-tab-shad"></i></a></li>
									</ul>
								</div>
								<div class="clear"></div>
								<div class="tab-wrapper">
									
									<!-- BEGIN FIRST TAB -->
								  <div id="tab1" class="tab">
										
								    <p>
									  &nbsp;&nbsp;&nbsp;&nbsp;艾特是一家集研发和生产为一体的高新科技企业，坐落在河南省郑州市高新区国家大学科技园，我们的产品为配有艾特诊断系统的艾特试剂盒。公司秉承“专注肿瘤诊断，为健康保驾护航”的宗旨，致力于肿瘤标志物检测试剂盒和辅助诊断系统的研发和推广。我们的主要工作集中在肿瘤标志物、相应试剂盒及配套系统的开发和市场推广方面，同时保持团队所有成员的协作和分工。 </p>
								  </div>
									<!-- END FIRST TAB -->
									
									<!-- BEGIN SECOND TAB -->
									<div id="tab2" class="tab">
									
									    <p> <strong>企业理念</strong><br/>
									      ◇ 企业使命 
艾特将始终如一地坚持&ldquo;专注肿瘤诊断，为健康保驾护航&rdquo;的宗旨，向客户提供可靠性高的产品和优质的服务，提高生命质量，增进人类健康。</p><p> 
◇ 企业宗旨 
&ldquo;专注肿瘤诊断，为健康保驾护航&rdquo; <br></p><p>
◇ 企业愿景 
做智能化肿瘤早期筛查与辅助诊断第一品牌！</p><p> 
◇ 企业形象 
艾特致力于提升人群整体健康水平，真诚为广大人民服务，力争成为具有极高的公信度及美誉度的高科技公司。</p><p> 
◇ 企业口号 
&ldquo;艾特，让生活更美好！&rdquo; <br></p><p>
◇ 企业文化 
艾特倡导诚信为本、科技为民的伦理文化；以德为本、唯才是举的用人文化；艰苦创业，团结奉献、争做主人、力求创新的行为文化。 
7.4.1.2 经营理念 
◇ 经营方向 
艾特是主要从事肿瘤标志物检测试剂盒与配套的辅助诊断系统研发的高科技企业。</p><p> 
◇ 经营方针 
艾特将时刻贯彻&ldquo;质量优先、客户第一；信誉行前、真诚服务&rdquo;的经营方针。 <br></p><p>◇ 经营道德 
严守商誉、为客户提供最满意的服务！</p><p> 
◇ 经营理念 
&ldquo;以人为本&rdquo;的人才管理观 
&ldquo;客户至上&rdquo;的营销管理观 
&ldquo;科技领先&rdquo;的研发管理观 
&ldquo;讲求细节&rdquo;的质量管理观 
&ldquo;公开透明&rdquo;的财务管理观 
 
</p>
										
										
										<br/>
									</div>
									<!-- END SECOND TAB -->
									
									<!-- BEGIN THIRD TAB -->
									<div id="tab3" class="tab">
									  <p><strong>品牌理念</strong><br/>
									  艾特将通过系统的品牌工程，全力打造其自身的品牌概念，并通过多种手段将其传递到顾客的手中，力争给顾客传递以几种品牌价值。
◇ 信誉品牌
可靠的产品质量、完善的售后服务、与时俱进的经营理念，艾特将树立一个值得客户信赖的品牌形象。
◇ 服务品牌
时刻注意客户的需求、提供个性化的解决方案。注重整体产品概念，将产品当作服务来卖，建立完善的客户服务体系，注重服务营销与关系营销的运用。企业将在不断的市场竞争中成为真正的“急客户所急、想客户所想”的服务企业！
◇ 专业品牌
从企业VI 设计特征、技术人员水准、服务人员态度及规章制度等多个方面，体现出企业的专业化形象。

										</p>
										
								  </div>
									<!-- END THIRD TAB -->
									
									
									<!-- BEGIN FOURTH TAB -->
									<div id="tab4" class="tab">
										
										 <p><strong>商标寓意</strong><br/>
										①山峰样商标寓意着艾特公司将努力不断研发新产品，攀登商业高峰，。
② A代表Artificial，山峰寓意着倒着的T，代表Tumor，合起来代表我们的AT公司。
③同时山峰样图形也很像一面帆，寓指我们公司致力于为健康保驾护航。

</p>
										
									</div>
									<!-- END FOURTH TAB -->
								</div>
								
							</div>
							<!-- END TABBED CONTENT -->
							
						</div>
					</div>
					
				</section>
				
			</div>
			<!-- END CONTENT HOLDER -->
			
			
			<!-- BEGIN WIDGETS --><!-- END WIDGETS -->
			
			
			<!-- BEGIN FOOTER -->
			<footer id="footer">
				<div class="container">
					<div class="grid_12" align="center">
						<small>版权所有</small><a href="http://www.cssmoban.com/" title="网站模板" target="_blank"></a>
					</div>
				</div>
			</footer>
			<!-- END FOOTER -->
			
		
	</div>
	<!-- END WRAPPER -->
	
	
	<!-- BEGIN MODAL WINDOWS -->
	<!-- BEGIN MODAL WINDOWS -->
	<div id="signin" class="reveal-modal">
		<header class="reveal-modal-header">
			用户登录
		</header>
		<div class="cont">
			<div class="indication">
				<em id="e" style="color:red"/></em>
			</div>
			<form action="loginAction" class="signin" />
				<p class="clearfix">
					<label>邮箱:</label>
					<input type="text" name="user.uemail" id="email"/>
				</p>
				<p class="clearfix">
					<label>密码:</label>
					<input type="text" name="user.upassword" id="password"/>
				</p>
				<p class="checkboxes without-label clearfix">
					<a href="#">忘记密码?</a>
				</p>
				<p class="without-label clearfix"><input type="submit" value="" onClick="return loginCheck();"/></p>
			</form>
		</div>
		<a class="close-reveal-modal">&#215;</a>
	</div>

	<div id="signup" class="reveal-modal">
		<header class="reveal-modal-header">
			用户注册
		</header>
		
		<div class="cont">
			<form  action="registAction.action" method="post" class="signup"/>
				<p class="clearfix">
					<em id="error" style="color:red">(<abbr>*</abbr>)要求全部的字段</em>
					
				</p>
				<p class="clearfix">
					<label>邮箱:<abbr>*</abbr></label>
					<input type="text" name="user.uemail" id="ue"/>
				</p>
				<p class="clearfix">
					<label>昵称:<abbr>*</abbr></label>
					<input type="text"  name="user.uname" id="name"/>
				</p>
				<p class="clearfix">
					<label>密码:<abbr>*</abbr></label>
					<input type="text"  name="user.upassword" id="pwd"/>
				</p>
				<p class="clearfix">
					<label>重复密码:<abbr>*</abbr></label>
					<input type="text" name="rePassword" id="repwd"/>
				</p>
				
				<p class="checkboxes clearfix">
					<span class="niceCheck"><input type="checkbox" name="ch1" id="chh1"/></span>
					同意(阅读)条款
				</p>
				
				<p class="without-label clearfix"><input type="submit"  value="" onClick="return check();"/></p>
			
			</form>
		</div>
		<a class="close-reveal-modal">&#215;</a>
	</div>

	<!-- END MODAL WINDOWS -->
	
	
	<!-- Javascript Files
	================================================== -->
	
	<!-- initialize jQuery Library -->
	<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.1/jquery.min.js"></script>
	<script>window.jQuery || document.write('<script src="js/jquery-1.8.1.min.js"><\/script>')</script>
	<!-- Modernizr -->
	<script type="text/javascript" src="js/modernizr.custom.14583.js"></script>
	<!-- Superfish Menu -->
	<script type="text/javascript" src="js/superfish.js"></script>
	<!-- easing plugin -->
	<script type="text/javascript" src="js/jquery.easing.min.js"></script>
	<!-- Prettyphoto -->
	<script type="text/javascript" src="js/jquery.prettyPhoto.js"></script>
	<!-- Mobile Menu -->
	<script type="text/javascript" src="js/jquery.mobilemenu.js"></script>
	<!-- Twitter -->
	<script type="text/javascript" src="js/jquery.twitter.js"></script>
	<!-- Elastslide -->
	<script type="text/javascript" src="js/jquery.elastislide.js"></script>
	<!-- Custom Checkbox -->
	<script type="text/javascript" src="js/jquery.checkbox.js"></script>
	<!-- Flexslider -->
	<script type="text/javascript" src="js/jquery.flexslider.js"></script>
	<!-- Reveal Modal -->
	<script type="text/javascript" src="js/jquery.reveal.js"></script>
	<script type="text/javascript" src="js/login.js" charset="utf-8"></script>
	<!-- Custom -->
	<script type="text/javascript" src="js/custom.js"></script>
	
	<script type="text/javascript">

		var _gaq = _gaq || [];
		_gaq.push(['_setAccount', 'UA-34673973-1']);
		_gaq.push(['_trackPageview']);

		(function() {
			var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
			ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
			var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
		})();

	</script>
	
</body>
</html>