﻿<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8" import="java.util.*"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<!DOCTYPE html>
<!--[if IE 7]>                  <html class="ie7 no-js" lang="en">     <![endif]-->
<!--[if lte IE 8]>              <html class="ie8 no-js" lang="en">     <![endif]-->
<!--[if (gte IE 9)|!(IE)]><!--> <html class="not-ie no-js" lang="en">  <!--<![endif]-->
<head>

	<!-- Basic Page Needs
  ================================================== -->
	<meta charset="utf-8" />
	<title>关于我们</title>
	<meta name="description" content="" />
	<meta name="author" content="" />
	
	<!-- Mobile Specific Metas
  ================================================== -->
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
	
	
	<!-- CSS
  ================================================== -->
	<!-- Normalize default styles -->
	<link rel="stylesheet" href="css/normalize.css" media="screen" />
	<!-- Fonts -->
	<link rel="stylesheet" href="css/fonts.css" media="screen" />
	<!-- Skeleton grid system -->
	<link rel="stylesheet" href="css/skeleton.css" media="screen" />
	<!-- Base Template Styles-->
	<link rel="stylesheet" href="css/base.css" media="screen" />
	<!-- Superfish Menu-->
	<link rel="stylesheet" href="css/superfish.css" media="screen" />
	<!-- Template Styles-->
	<link rel="stylesheet" href="css/style.css" media="screen" />
	<!-- PrettyPhoto -->
	<link rel="stylesheet" href="css/prettyPhoto.css" media="screen" />
	<!-- Flexslider -->
	<link rel="stylesheet" href="css/flexslider.css" media="screen" />
	<!-- Layout and Media queries-->
	<link rel="stylesheet" href="css/layout.css" media="screen" />
	<!--[if lt IE 9]>
		<link rel="stylesheet" href="css/ie/ie8.css" media="screen" />
	<![endif]-->
	
	
	<!--[if lt IE 9]>
		<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
	<![endif]-->
	
	
	<!-- Favicons
	================================================== -->
	<link rel="shortcut icon" href="images/favicon.ico" />
	<link rel="apple-touch-icon" href="images/apple-touch-icon.png" />
	<link rel="apple-touch-icon" sizes="72x72" href="images/apple-touch-icon-72x72.png" />
	<link rel="apple-touch-icon" sizes="114x114" href="images/apple-touch-icon-114x114.png" />
	
	
	<!-- For Old Browsers
	================================================== -->
	<!--[if lt IE 8]>
	<div style=' clear: both; text-align:center; position: relative;'>
		<a href="http://www.microsoft.com/windows/internet-explorer/default.aspx?ocid=ie6_countdown_bannercode"><img src="http://storage.ie6countdown.com/assets/100/images/banners/warning_bar_0000_us.jpg" border="0" alt="" /></a>
	</div>
	<![endif]-->
	
	
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" /></head>
<body class="page single-post-page">

	<!-- Primary Page Layout
	================================================== -->
	
	<!-- BEGIN WRAPPER -->
	<div id="wrapper">
			
			
			<!-- BEGIN HEADER -->
			<header id="header">
				
				<!--Top Header-->
				<div id="top-header">
					<div class="container">
						<div class="grid_12">
							
							<!-- Begin Language Switcher -->
								欢迎使用
							<!-- End Language Switcher 当前用户：-->

							<div class="login-holder fright">
							<% 
								Object sv=session.getAttribute("uname"); 
								if(sv!=null){
								
								%> 用户名：<font color="red">${sessionScope.uname}</font><a href=logout.jsp>退出</a>
								<%
								}
								else{
								 %>
								<a href="#" class="signin" data-reveal-id="signin">登录</a>
								<a href="#" class="signin" data-reveal-id="signup">注册</a>
								<%} %>
							</div>
						</div>
					</div>
				</div>
				
				<!--Main Header-->
				<div id="main-header">
					<div class="container">
						<div class="grid_12">
						
							<!-- BEGIN LOGO -->
							<div id="logo">
								<!-- Image based Logo-->
								<a href="index.jsp"><img src="images/logo.png" alt="" /></a>
								
								
								<!-- Text based Logo
								<h1><a href="index.html"><span>此处</span>可以添加标题文字</a></h1>
								<p class="tagline">请添加标题文字</p>
								-->
								
							</div>
							<!-- END LOGO -->
							
							<!-- BEGIN NAVIGATION -->
							<nav class="primary">
								<ul class="sf-menu">
									<li class="current-menu-item"><a href="index.jsp">首&nbsp;&nbsp;&nbsp;&nbsp;页</a></li>
									<li><a href="#">产品介绍</a>
										<ul>
											<li><a href="product.jsp">产品特征</a></li>
											<li><a href="product1.jsp">核心技术</a></li>
											<li><a href="product2.jsp">产品研发</a></li>
											<li><a href="product3.jsp">市场分析</a></li>
										</ul>
									</li>
									<li><a href="company.jsp">公司概况</a>
									</li>
									<li><a href="about-us.jsp">关于我们</a>
									<ul>
											<li><a href="contact-us.jsp">联系我们</a></li>
									</ul>
									</li>
									<li><a href="#">进入后台</a></li>
									
								</ul>
							</nav>
							<!-- END NAVIGATION -->
							
						</div>
					</div>
				</div>
				
			</header>
			<!-- END HEADER -->
			
			
			
			<!-- BEGIN PAGE HEADING -->
			<section id="heading">
				
				<div class="container">
						
					<div class="grid_8">
						<!-- BEGIN PAGE HEADING -->
						<header class="page-heading">
							<h1>关于我们</h1>
							<nav class="breadcrumbs">
								<ul>
									<li><a href="index.jsp">首页</a></li>
									<li class="current">关于我们</li>
								</ul>
							</nav>
						</header>
						<!-- END PAGE HEADING -->
					</div>
					
					<div class="grid_4">
						<div class="prefix_1_2">
							<!-- BEGIN SEARCH FORM -->
							<form id="search-form" class="search-form" action="#" />
								<input type="text" placeholder="搜索..." />
								<input type="submit" value="" />
							</form>
							<!-- END SEARCH FORM -->
						</div>
					</div>
						
				</div>
				
			</section>
			<!-- END PAGE HEADING -->
			
			
			
			<!-- BEGIN CONTENT HOLDER -->
			<div id="content-wrapper">
				
				<section class="indent">
					
					<div class="container">
						
						<div class="grid_12">
							
							<h2>艾特啄木鸟团队</h2>
							<p><strong>“艾特啄木鸟团队”</strong>啄木鸟团队是一支高学历、复合型的队伍。目前团队共有8名成员，分别来自四个不同的年级，四个不同的院系，涵盖了预防医学、财务管理、会计学、软件开发、法学等不同的专业领域。其中5名应届同学都已报送至北京大学、复旦大学、华中科技大学等名校。
啄木鸟团队是一支勇于创新、积极进取、不断成长的创业团队。每一位成员都具有深厚的专业技术基础，深邃的战略眼光和谋略，强烈的事业心、良好的亲和力以及有非常好的团队合作意识。如今我们在团队中互相激励、发挥优势互补，构成了一支具备一定实力和极强凝聚力的精英团队。
</p>
<img src="images/p0.jpg" alt="" width="600" height="286" border="0"  />
							
						
						</div>
						
					</div>
					
				</section>
				
			</div>
			<!-- END CONTENT HOLDER -->
			
			
			<!-- BEGIN WIDGETS --><!-- END WIDGETS -->
			
			
			<!-- BEGIN FOOTER -->
			<footer id="footer">
				<div class="container">
					<div class="grid_12" align="center">
						<small>版权所有</small>
					</div>
				</div>
			</footer>
			<!-- END FOOTER -->
			
		
	</div>
	<!-- END WRAPPER -->
	
	
	<!-- BEGIN MODAL WINDOWS -->
	<div id="signin" class="reveal-modal">
		<header class="reveal-modal-header">
			用户登录
		</header>
		<div class="cont">
			<div class="indication">
				<em id="e" style="color:red"/></em>
			</div>
			<form action="loginAction" class="signin" />
				<p class="clearfix">
					<label>邮箱:</label>
					<input type="text" name="user.uemail" id="email"/>
				</p>
				<p class="clearfix">
					<label>密码:</label>
					<input type="text" name="user.upassword" id="password"/>
				</p>
				<p class="checkboxes without-label clearfix">
					<a href="#">忘记密码?</a>
				</p>
				<p class="without-label clearfix"><input type="submit" value="" onClick="return loginCheck();"/></p>
			</form>
		</div>
		<a class="close-reveal-modal">&#215;</a>
	</div>

	<div id="signup" class="reveal-modal">
		<header class="reveal-modal-header">
			用户注册
		</header>
		
		<div class="cont">
			<form  action="registAction.action" method="post" class="signup"/>
				<p class="clearfix">
					<em id="error" style="color:red">(<abbr>*</abbr>)要求全部的字段</em>
					
				</p>
				<p class="clearfix">
					<label>邮箱:<abbr>*</abbr></label>
					<input type="text" name="user.uemail" id="ue"/>
				</p>
				<p class="clearfix">
					<label>昵称:<abbr>*</abbr></label>
					<input type="text"  name="user.uname" id="name"/>
				</p>
				<p class="clearfix">
					<label>密码:<abbr>*</abbr></label>
					<input type="text"  name="user.upassword" id="pwd"/>
				</p>
				<p class="clearfix">
					<label>重复密码:<abbr>*</abbr></label>
					<input type="text" name="rePassword" id="repwd"/>
				</p>
				
				<p class="checkboxes clearfix">
					<span class="niceCheck"><input type="checkbox" name="ch1" id="chh1"/></span>
					同意(阅读)条款
				</p>
				
				<p class="without-label clearfix"><input type="submit"  value="" onClick="return check();"/></p>
			
			</form>
		</div>
		<a class="close-reveal-modal">&#215;</a>
	</div>
	<!-- END MODAL WINDOWS -->
	
	
	<!-- Javascript Files
	================================================== -->
	
	<!-- initialize jQuery Library -->
	<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.1/jquery.min.js"></script>
	<script>window.jQuery || document.write('<script src="js/jquery-1.8.1.min.js"><\/script>')</script>
	<!-- Modernizr -->
	<script type="text/javascript" src="js/modernizr.custom.14583.js"></script>
	<!-- Superfish Menu -->
	<script type="text/javascript" src="js/superfish.js"></script>
	<!-- easing plugin -->
	<script type="text/javascript" src="js/jquery.easing.min.js"></script>
	<!-- Prettyphoto -->
	<script type="text/javascript" src="js/jquery.prettyPhoto.js"></script>
	<!-- Mobile Menu -->
	<script type="text/javascript" src="js/jquery.mobilemenu.js"></script>
	<!-- Twitter -->
	<script type="text/javascript" src="js/jquery.twitter.js"></script>
	<!-- Elastslide -->
	<script type="text/javascript" src="js/jquery.elastislide.js"></script>
	<!-- Custom Checkbox -->
	<script type="text/javascript" src="js/jquery.checkbox.js"></script>
	<!-- Flexslider -->
	<script type="text/javascript" src="js/jquery.flexslider.js"></script>
	<!-- Reveal Modal -->
	<script type="text/javascript" src="js/jquery.reveal.js"></script>
	
	<script type="text/javascript" src="js/login.js" charset="utf-8"></script>
	<!-- Custom -->
	<script type="text/javascript" src="js/custom.js"></script>
	
	<script type="text/javascript">

		var _gaq = _gaq || [];
		_gaq.push(['_setAccount', 'UA-34673973-1']);
		_gaq.push(['_trackPageview']);

		(function() {
			var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
			ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
			var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
		})();

	</script>
	
</body>
</html>