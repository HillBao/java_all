<%@ page language="java" contentType="text/html; charset=GB18030"
    pageEncoding="GB18030"%>
    <%@taglib prefix="s" uri="/struts-tags"%>
    <%@ taglib prefix="page" uri="../WEB-INF/mytld/pagetag.tld"%>
    
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=GB18030"> 
<title>用户信息管理 </title>
<script type="text/javascript">
function deleteChecked(){
flag = false;
var de = document.getElementsByName("ids");
for(var i = 0; i < de.length; i++){
if(de[i].checked == true){
flag = true;
break;
} 
}
if(flag == false){
alert("至少选择一个待删除记录");
return false;
}
if(!confirm("确定删除?")) {
return false;
}
}
</script>
</head>
<body>
<form action="delChecked.action" method="post">
<table border="1" >
 <tr>
   <th>用户名</th>
   <th>性别</th>
   <th>邮箱</th>
   <th>c1</th>
   <th colspan="2">操作</th>
 </tr>
  <s:iterator value="#request.patientList" id="p">
 <tr align="center">
  <td><s:property value="#p.pname"/></td>
   <td><s:property value="#p.pname"/></td>    
   <td><s:property value="#p.pgender"/></td>    
   <td><s:property value="#p.pemail"/></td>    
   <td><s:property value="#p.c1"/></td>    
	<td>
	<s:a href="preUpdateAction.action?pid=%{#p.pid}">修改</s:a>
	</td>
	<td>
	<input type="checkbox" name="ids" value="${p.pid}">
	<s:a href="deleteAction.action?pid=%{#p.pid}" onclick="return confirm('确定删除该用户吗？');">删除</s:a>
	</td>
 </tr>
 </s:iterator>
     <tr>
					<td colspan="6" align="right">
					<input type="submit"  value="删除所选记录" onclick="return deleteChecked();"/>
					</td>
	</tr>
</table>
</form>


				<!-- 自定义的分页标签 -->
			
				<page:page pager="${pb}" />
			
</body>
</html>