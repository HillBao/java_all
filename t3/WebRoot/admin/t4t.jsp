<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    <%@ taglib prefix="s" uri="/struts-tags"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
<!-- 
<script type="text/javascript">
  function validate()
  {
	  //使用正则表达式去除前后空格
	    String.prototype.Trim = function(){
		  return this.replace(/^\s+|\s+$/g,"");
		 }
		  var name = document.getElementById("name").value.Trim();
		  var psw = document.getElementById("psw").value;
		  var repsw = document.getElementById("repsw").value;
	  if(name.length==0)
	  {
		  document.getElementById("error").innerHTML="用户名不能为空";
		  return false;
	  }
	  if(psw.length==0)
	  {
		  document.getElementById("error").innerHTML="密码不能为空";
		  return false;
	  }
	  if(psw!=repsw)
	  {
		  document.getElementById("error").innerHTML="密码和重复密码不一致，请重新输入";
		  return false;
	  }
	  return true;
  }
</script>
 -->
</head>
<body>
<center>
   <form action="updateAction.action" method="post">
    <table>
    <tr>
	    <th colspan="3">用户修改</th>
    </tr>
     <tr>
              <td colspan="3"> 
               <div id="error" style="color:red"></div>
               </td>
    </tr>
    <tr>
    <td>用户名</td>
    <td><input type="text" name="patient.pname" id="name" value="${request.p.pname}"/></td>
    <td></td>
    </tr>
    <tr>
    <td>性别</td>
    <td>
      <s:radio list="{'男','女'}" name="patient.pgender"  value="#request.p.pgender" theme="simple"/>
    </td>
    <td></td>
    </tr>

    <tr>
    <td>邮箱</td>
    <td><input type="text" name="patient.pemail" value="${request.p.pemail }"/></td>
    <td></td>
    </tr>
    <tr>
    <td>c1</td>
    <td><input type="text" name="patient.c1" id="c1" value="${request.p.c1}"/></td>
    <td></td>
    </tr>
    <tr>
    <td>c2</td>
    <td><input type="text" name="patient.c2" id="c2" value="${request.p.c2}"/></td>
    <td></td>
    </tr>
    <tr>
    <td>c3</td>
    <td><input type="text" name="patient.c2" id="c2" value="${request.p.c2}"/></td>
    <td></td>
    </tr>
    
    <tr>
    <td>c4</td>
    <td><input type="text" name="patient.c2" id="c2" value="${request.p.c2}"/></td>
    <td></td>
    </tr>
     
    <tr>
    <td>c5</td>
    <td><input type="text" name="patient.c2" id="c2" value="${request.p.c2}"/></td>
    <td></td>
    </tr>
    
    <tr>
    <td>c6</td>
    <td><input type="text" name="patient.c2" id="c2" value="${request.p.c2}"/></td>
    <td></td>
    </tr>
    <tr>
    <td>
    <input type="submit" value="修改" >
    </td>
    <td>  
      <input type="button" value="返回" onclick="javascript:history.back();">
    </td>
      <input type="hidden" name="patient.pid" value="${request.p.pid}"/>
    </tr>
    </table>
   </form>
</center>
</body>
</html>