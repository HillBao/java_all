package com.dao;


import java.util.List;

import com.bean.User;
import com.page.Pager;

public interface UserDao {

	public void save(User u);//插入新用户
	public boolean checkLogin1(String name,String password);//根据用户名和密码判断用户是否存在
	public String findUnameByEmail(String uemail);//根据邮箱查询用户
	public boolean checkLogin(String password,String email);//根据用户名和密码判断用户是否存在
	public String checkIdentity(String name,String password);
	public boolean checkLogin2(String uemail);//判断邮箱是否已经被注册

	public List<User> queryAllUser(Pager pager);//根据分页范围查询
	public int getTotalRows();//获取数据库的总记录数
}
