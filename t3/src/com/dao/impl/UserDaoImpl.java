package com.dao.impl;


import java.util.List;

import org.hibernate.Session;
import com.bean.User;
import com.dao.UserDao;
import com.page.Pager;
import com.util.HibernateSessionFactory;

public class UserDaoImpl implements UserDao {

	@Override
	public boolean checkLogin1(String name, String password) {
		Session session = HibernateSessionFactory.getSession();
		session.beginTransaction();
		String strHql = "select count(*) from User u where "
				+ "u.uname=:a and u.upassword=:b";
		Long count = (Long) session.createQuery(strHql).setParameter("a", name)
				.setParameter("b", password).iterate().next();
		System.out.println("count = " + count);
		boolean b = false;
		if (count > 0)
			b = true;
		session.getTransaction().commit();
		HibernateSessionFactory.closeSession();
		return b;
	}


	@Override
	public String checkIdentity(String name, String password) {
		// TODO Auto-generated method stub
		Session session = HibernateSessionFactory.getSession();
		session.beginTransaction();
		String strHql = "select uidentity from User u where "
			+ "u.uname=:a and u.upassword=:b";
		String identity = (String) session.createQuery(strHql).setParameter("a", name)
		.setParameter("b", password).iterate().next();
		session.getTransaction().commit();
		HibernateSessionFactory.closeSession();
		return identity;
	}

	@Override
	public void save(User u) {
		Session session = HibernateSessionFactory.getSession();
		session.beginTransaction();
		session.save(u);
		session.getTransaction().commit();
		HibernateSessionFactory.closeSession();
	}


	@Override
	public int getTotalRows() {////获取数据库的总记录数
		Session session = HibernateSessionFactory.getSession();
		int totalRows = 0;
		String strHql = "select count(*) from User";
		Object obj = session.createQuery(strHql).list().iterator().next();
		if (obj != null)
			totalRows = Integer.parseInt(obj.toString());
		HibernateSessionFactory.closeSession();
		return totalRows;
	}


	@Override
	public List<User> queryAllUser(Pager pager) {
		Session session = HibernateSessionFactory.getSession();
		session.beginTransaction();
		String strHql = "from User";
		List<User> list = session.createQuery(strHql).list();
		session.getTransaction().commit();
		HibernateSessionFactory.closeSession();
		return list;
	}


	@Override
	public boolean checkLogin(String password,String email) {//密码，邮箱验证
		Session session = HibernateSessionFactory.getSession();
		session.beginTransaction();
		String strHql = "select count(*) from User u where "
				+ "u.upassword=:a and u.uemail=:b";
		Long count = (Long) session.createQuery(strHql).setParameter("a", password)
				.setParameter("b", email).iterate().next();
		System.out.println("count = " + count);
		boolean b = false;
		if (count > 0)
			b = true;
		session.getTransaction().commit();
		HibernateSessionFactory.closeSession();
		return b;
	}


	@Override
	public String findUnameByEmail(String uemail) {
		Session session = HibernateSessionFactory.getSession();
		session.beginTransaction();
		String strHql = "select uname from User u where "
			+ "u.uemail=:a";
		String uname = (String) session.createQuery(strHql).setParameter("a", uemail).iterate().next();
		session.getTransaction().commit();
		HibernateSessionFactory.closeSession();
		return uname;
	}


	@Override
	public boolean checkLogin2(String uemail) {
		Session session = HibernateSessionFactory.getSession();
		session.beginTransaction();
		String strHql = "select count(*) from User u where "
				+ "u.uemail=:a";
		Long count = (Long) session.createQuery(strHql).setParameter("a", uemail).iterate().next();
		boolean b = true;
		if (count > 0)
			b = false;
		session.getTransaction().commit();
		HibernateSessionFactory.closeSession();
		return b;
	}


}
