package com.bean;

import javax.persistence.*;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.GenericGenerator;

@Entity
@Table(name = "user")
//@Cache(usage = CacheConcurrencyStrategy.READ_ONLY)
public class User {

	@Id
	@Column(name = "id")
	@GenericGenerator(name = "myGen",strategy = "native")
	@GeneratedValue(generator = "myGen")
	private Integer uid;
	@Column(name = "name",length = 20)
	private String uname;
	@Column(name = "password")
	private String upassword;
	@Column(name = "email")
	private String uemail;
	
	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return " ��ţ�"+uid+"  ������"+uname
		+" ���룺"+upassword+"  ����"+uemail;
	}
	
	public User() {
		super();
	}

	public User(String uname, String upassword, String uemail) {
		super();
		this.uname = uname;
		this.upassword = upassword;
		this.uemail = uemail;
	}
	
	public Integer getUid() {
		return uid;
	}

	public void setUid(Integer uid) {
		this.uid = uid;
	}
	
	public String getUname() {
		return uname;
	}

	public void setUname(String uname) {
		this.uname = uname;
	}
	
	public String getUpassword() {
		return upassword;
	}

	public void setUpassword(String upassword) {
		this.upassword = upassword;
	}

	public String getUemail() {
		return uemail;
	}

	public void setUemail(String uemail) {
		this.uemail = uemail;
	}
	

}
