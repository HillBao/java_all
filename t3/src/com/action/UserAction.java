package com.action;


import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts2.ServletActionContext;

import com.bean.User;
import com.dao.UserDao;
import com.dao.impl.UserDaoImpl;
import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;
import com.page.Pager;
import com.page.PagerHelper;

public class UserAction extends ActionSupport {

	private User user;
	private UserDao userDao = new UserDaoImpl();
	private String validateCode;

	


	public String getValidateCode() {
		return validateCode;
	}

	public void setValidateCode(String validateCode) {
		this.validateCode = validateCode;
	}

	public UserDao getUserDao() {
		return userDao;
	}

	public void setUserDao(UserDao userDao) {
		this.userDao = userDao;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	//ע��action
	public String registAction() throws Exception {
		String strReturn = INPUT;
		if(this.userDao.checkLogin2(this.user.getUemail())){
			this.userDao.save(this.user);
			strReturn = SUCCESS;
		}else {
				this.addActionMessage("�����Ѿ���ע�ᣡ");
			}
		return strReturn;
	}

	
	// ��¼action
	public String loginAction() throws Exception {
		String strReturn = INPUT;
			if (this.userDao.checkLogin(this.user.getUpassword(), this.user.getUemail())) {
					strReturn = SUCCESS;
					String uname = (String)(this.userDao.findUnameByEmail(this.user.getUemail()));
					ActionContext.getContext().getSession().put("uname",
							uname);

			} else {
				this.addActionMessage("��¼ʧ�ܣ�������������");
			}

		return strReturn;
	}
	
}
