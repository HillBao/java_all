package admin.dao.impl;

import java.util.List;

import org.hibernate.Session;

import admin.bean.Patient;
import admin.bean.Pcase;
import admin.dao.PcaseDao;

import com.page.Pager;
import com.util.HibernateSessionFactory;

public class PcaseDaoImpl implements PcaseDao {


	public void delete(Pcase p) {
		Session session = HibernateSessionFactory.getSession();
		session.beginTransaction();
		session.delete(p);
		session.getTransaction().commit();
		HibernateSessionFactory.closeSession();

	}

	public Pcase findPcaseById(Integer id) {
		Session session = HibernateSessionFactory.getSession();
		session.beginTransaction();
		Pcase p = (Pcase) session.get(Pcase.class, id);
		session.getTransaction().commit();
		HibernateSessionFactory.closeSession();
		return p;
	}
 

	public void save(Pcase p) {
		Session session = HibernateSessionFactory.getSession();
		session.beginTransaction();
		session.save(p);
		session.getTransaction().commit();
		HibernateSessionFactory.closeSession();
	}

	public void update(Pcase p) {
		Session session = HibernateSessionFactory.getSession();
		session.beginTransaction();
		session.update(p);
		session.getTransaction().commit();
		HibernateSessionFactory.closeSession();

	}

	public int getTotalRows() {
		Session session = HibernateSessionFactory.getSession();
		int totalRows = 0;
		String strHql = "select count(*) from Pcase";
		Object obj = session.createQuery(strHql).list().iterator().next();
		if (obj != null)
			totalRows = Integer.parseInt(obj.toString());
		HibernateSessionFactory.closeSession();
		return totalRows;

	}

 
 

	@Override
	public List<Pcase> queryAllPcase(Pager pager) {
		Session session = HibernateSessionFactory.getSession();
		List<Pcase> list = session.createQuery("from Pcase").setFirstResult(
				pager.getStartRow()).setMaxResults(pager.getPageSize()).list();
		HibernateSessionFactory.closeSession();
		return list;
	}

	@Override
	public List<Pcase> queryAllPcase() {
		Session session = HibernateSessionFactory.getSession();
		session.beginTransaction();
		String strHql = "from Pcase";
		List<Pcase> myList = session.createQuery(strHql).list();
		session.getTransaction().commit();
		HibernateSessionFactory.closeSession();
		return myList;
	}

	@Override
	public int getCountBySearch(int searchType, String searchValue) {
		Session session = HibernateSessionFactory.getSession();
		session.beginTransaction();
		int totalRows = 0;
		String strHql = "select count(*) from Pcase as p  where p.";
		Object obj = null;
		// 用户名
		if (searchType == 1) {
			strHql += "pname";
		}
		// 性别
		else if (searchType == 2) {
			strHql += "phospital";
		}
		strHql += " like:searchValue";
		obj = session.createQuery(strHql)
		.setParameter("searchValue","%"+searchValue+"%")
		.iterate().next();

		if (obj != null)
			totalRows = Integer.parseInt(obj.toString());
		session.getTransaction().commit();
		HibernateSessionFactory.closeSession();
		return totalRows;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Pcase> queryBySearch(int searchType, String searchValue,
			Pager pager) {
		Session session = HibernateSessionFactory.getSession();
		session.beginTransaction();
		String str = "from Pcase as p  where p.";
		List<Pcase> list = null;
		// 根据用户名查询
		if (searchType == 1) {
			str += "pname like:searchValue";
		}
		// 性别
		else if (searchType == 2) {
			str += "phospital like:searchValue";
		}

		list = session.createQuery(str)
				.setParameter("searchValue", "%"+searchValue+"%").setFirstResult(
						pager.getStartRow()).setMaxResults(pager.getPageSize())
				.list();
		session.getTransaction().commit();
		HibernateSessionFactory.closeSession();
		return list;
	}
}
