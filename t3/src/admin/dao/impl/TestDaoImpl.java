package admin.dao.impl;

import java.util.List;

import org.hibernate.Session;

import admin.bean.Patient;
import admin.bean.Pcase;
import admin.bean.Test;
import admin.dao.PcaseDao;
import admin.dao.TestDao;

import com.bean.User;
import com.page.Pager;
import com.util.HibernateSessionFactory;

public class TestDaoImpl implements TestDao {


 

 
	public int getTotalRows() {
		Session session = HibernateSessionFactory.getSession();
		int totalRows = 0;
		String strHql = "select count(*) from Test";
		Object obj = session.createQuery(strHql).list().iterator().next();
		if (obj != null)
			totalRows = Integer.parseInt(obj.toString());
		HibernateSessionFactory.closeSession();
		return totalRows;

	}


	public int getCountBySearch(String searchValue) {
		Session session = HibernateSessionFactory.getSession();
		session.beginTransaction();
		int totalRows = 0;
		String strHql = "select count(*) from Test as p  where p.";
		Object obj = null;
		strHql += "tname";
		strHql += " like:searchValue";
		obj = session.createQuery(strHql)
		.setParameter("searchValue","%"+searchValue+"%")
		.iterate().next();

		if (obj != null)
			totalRows = Integer.parseInt(obj.toString());
		session.getTransaction().commit();
		HibernateSessionFactory.closeSession();
		return totalRows;
	}

	@SuppressWarnings("unchecked")
	public List<Test> queryBySearch(String searchValue,
			Pager pager) {
		Session session = HibernateSessionFactory.getSession();
		session.beginTransaction();
		String str = "from Test as p  where p.";
		List<Test> list = null;
		str += "tname like:searchValue";
		list = session.createQuery(str)
				.setParameter("searchValue", "%"+searchValue+"%").setFirstResult(
						pager.getStartRow()).setMaxResults(pager.getPageSize())
				.list();
		session.getTransaction().commit();
		HibernateSessionFactory.closeSession();
		return list;
	}
  
 
 

	@Override
	public void delete(Test t) {
		Session session = HibernateSessionFactory.getSession();
		session.beginTransaction();
		session.delete(t);
		session.getTransaction().commit();
		HibernateSessionFactory.closeSession();
		
	}

	@Override
	public Test findTestById(Integer id) {
		Session session = HibernateSessionFactory.getSession();
		session.beginTransaction();
		Test p = (Test) session.get(Test.class, id);
		session.getTransaction().commit();
		HibernateSessionFactory.closeSession();
		return p;
	}

	@Override
	public List<Test> queryAllTest() {
		Session session = HibernateSessionFactory.getSession();
		session.beginTransaction();
		String strHql = "from Test";
		List<Test> myList = session.createQuery(strHql).list();
		session.getTransaction().commit();
		HibernateSessionFactory.closeSession();
		return myList;
	}

	@Override
	public List<Test> queryAllTest(Pager pager) {
		Session session = HibernateSessionFactory.getSession();
		List<Test> list = session.createQuery("from Test").setFirstResult(
				pager.getStartRow()).setMaxResults(pager.getPageSize()).list();
		HibernateSessionFactory.closeSession();
		return list;
	}

	@Override
	public void save(Test t) {
		Session session = HibernateSessionFactory.getSession();
		session.beginTransaction();
		session.save(t);
		session.getTransaction().commit();
		HibernateSessionFactory.closeSession();
		
	}

	@Override
	public void update(Test t) {
		Session session = HibernateSessionFactory.getSession();
		session.beginTransaction();
		session.update(t);
		session.getTransaction().commit();
		HibernateSessionFactory.closeSession();
		
	}


	@Override
	public int getCountBySearch(int searchType, String searchValue) {
		Session session = HibernateSessionFactory.getSession();
		session.beginTransaction();
		int totalRows = 0;
		String strHql = "select count(*) from Test as p  where p.";
		Object obj = null;
		// 用户名
		if (searchType == 1) {
			strHql += "tname";
		}
		// 性别
		else if (searchType == 2) {
			strHql += "cresult";
		}
		else if (searchType == 3) {
			strHql += "presult";
		}
		strHql += " like:searchValue";
		obj = session.createQuery(strHql)
		.setParameter("searchValue","%"+searchValue+"%")
		.iterate().next();
		if (obj != null)
			totalRows = Integer.parseInt(obj.toString());
		session.getTransaction().commit();
		HibernateSessionFactory.closeSession();
		return totalRows;
	}


	@Override
	public List<Test> queryBySearch(int searchType, String searchValue,
			Pager pager) {
		Session session = HibernateSessionFactory.getSession();
		session.beginTransaction();
		String str = "from Test as p  where p.";
		List<Test> list = null;
		// 根据用户名查询
		if (searchType == 1) {
			str += "tname like:searchValue";
		}
		// 性别
		else if (searchType == 2) {
			str += "cresult like:searchValue";
		}
		else if (searchType == 3) {
			str += "presult like:searchValue";
		}

		list = session.createQuery(str)
				.setParameter("searchValue", "%"+searchValue+"%").setFirstResult(
						pager.getStartRow()).setMaxResults(pager.getPageSize())
				.list();
		session.getTransaction().commit();
		HibernateSessionFactory.closeSession();
		return list;
	}


	@Override
	public List<Test> searchOne(String name) {
		Session session = HibernateSessionFactory.getSession();
		session.beginTransaction();
		String strHql = "from Test as p  where p.";
		List<Test> obj = null;
		strHql += "tname";
		strHql += " =:name";
		obj = session.createQuery(strHql)
		.setParameter("name",name)
		.list();
		session.getTransaction().commit();
		HibernateSessionFactory.closeSession();
		return obj;
	}
 
}
