package admin.dao.impl;

import java.sql.Date;
import java.util.List;

import org.hibernate.Session;

import admin.bean.Patient;
import admin.dao.PatientDao;

import com.bean.User;
import com.page.Pager;
import com.util.HibernateSessionFactory;

public class PatientDaoImpl implements PatientDao {



	public void delete(Patient p) {
		Session session = HibernateSessionFactory.getSession();
		session.beginTransaction();
		session.delete(p);
		session.getTransaction().commit();
		HibernateSessionFactory.closeSession();

	}

	public Patient findPatientById(Integer id) {
		Session session = HibernateSessionFactory.getSession();
		session.beginTransaction();
		Patient p = (Patient) session.get(Patient.class, id);
		session.getTransaction().commit();
		HibernateSessionFactory.closeSession();
		return p;
	}

	@Override
	public List<Patient> queryAllPatient() {
		Session session = HibernateSessionFactory.getSession();
		session.beginTransaction();
		String strHql = "from Patient";
		List<Patient> myList = session.createQuery(strHql).list();
		session.getTransaction().commit();
		HibernateSessionFactory.closeSession();
		return myList;
	}

	@Override
	public void save(Patient p) {
		Session session = HibernateSessionFactory.getSession();
		session.beginTransaction();
		session.save(p);
		session.getTransaction().commit();
		HibernateSessionFactory.closeSession();
	}

	@Override
	public void update(Patient p) {
		Session session = HibernateSessionFactory.getSession();
		session.beginTransaction();
		session.update(p);
		session.getTransaction().commit();
		HibernateSessionFactory.closeSession();

	}

	@Override
	public int getTotalRows() {
		Session session = HibernateSessionFactory.getSession();
		int totalRows = 0;
		String strHql = "select count(*) from Patient";
		Object obj = session.createQuery(strHql).list().iterator().next();
		if (obj != null)
			totalRows = Integer.parseInt(obj.toString());
		HibernateSessionFactory.closeSession();
		return totalRows;

	}

 


	@Override
	public List<Patient> queryAllPatient(Pager pager) {
		Session session = HibernateSessionFactory.getSession();
		List<Patient> list = session.createQuery("from Patient").setFirstResult(
				pager.getStartRow()).setMaxResults(pager.getPageSize()).list();
		HibernateSessionFactory.closeSession();
		return list;
	}

	@Override
	public int getCountBySearch(int searchType, String searchValue) {
		Session session = HibernateSessionFactory.getSession();
		session.beginTransaction();
		int totalRows = 0;
		String strHql = "select count(*) from Patient as p  where p.";
		Object obj = null;
		// 用户名
		if (searchType == 1) {
			strHql += "pname";
		}
		// 性别
		else if (searchType == 2) {
			strHql += "phospital";
		}
		strHql += " like:searchValue";
		obj = session.createQuery(strHql)
		.setParameter("searchValue","%"+searchValue+"%")
		.iterate().next();

		if (obj != null)
			totalRows = Integer.parseInt(obj.toString());
		session.getTransaction().commit();
		HibernateSessionFactory.closeSession();
		return totalRows;
	}
	@SuppressWarnings("unchecked")
	@Override
	public List<Patient> queryBySearch(int searchType, String searchValue,
			Pager pager) {
		Session session = HibernateSessionFactory.getSession();
		session.beginTransaction();
		String str = "from Patient as p  where p.";
		List<Patient> list = null;
		// 根据用户名查询
		if (searchType == 1) {
			str += "pname like:searchValue";
		}
		// 性别
		else if (searchType == 2) {
			str += "phospital like:searchValue";
		}

		list = session.createQuery(str)
				.setParameter("searchValue", "%"+searchValue+"%").setFirstResult(
						pager.getStartRow()).setMaxResults(pager.getPageSize())
				.list();
		session.getTransaction().commit();
		HibernateSessionFactory.closeSession();
		return list;
	}

	@Override
	public List<Patient> searchOne(String name,String phospital,String pgender,String num) {
		Session session = HibernateSessionFactory.getSession();
		session.beginTransaction();
		String strHql = "from Patient as p  where p.";
		List<Patient> obj = null;
		strHql += "pname";
		strHql += " =:name and p.";
		strHql += "pgender";
		strHql += "=:pgender";
		strHql += " and p.";
		strHql += "pnumber";
		strHql += "=:pnumber";
		strHql += " and p.";
		strHql += "phospital";
		strHql += " like:phospital";
		obj = session.createQuery(strHql)
		.setParameter("name",name)
		.setParameter("pgender",pgender)
		.setParameter("pnumber",num)
		.setParameter("phospital","%"+phospital+"%")
		.list();
		session.getTransaction().commit();
		HibernateSessionFactory.closeSession();
		return obj;
	}
//根据医院和日期查询患者
	@Override
	public List<Patient> searchByhostpital(String phospital, Date d) {
		Session session = HibernateSessionFactory.getSession();
		session.beginTransaction();
		String strHql = "from Patient as p  where p.";
		List<Patient> obj = null;
		strHql += "ptime";
		strHql += " =:t and p.";
		strHql += "phospital";
		strHql += "=:phospital";
		obj = session.createQuery(strHql)
		.setParameter("t",d)
		.setParameter("phospital",phospital)
		.list();
		session.getTransaction().commit();
		HibernateSessionFactory.closeSession();
		return obj;
	}


//	@Override
//	public List<Patient> searchOne(String name) {
//		Session session = HibernateSessionFactory.getSession();
//		session.beginTransaction();
//		String strHql = "from Patient as p  where p.";
//		List<Patient> obj = null;
//		strHql += "pname";
//		strHql += " =:name";
//		obj = session.createQuery(strHql)
//		.setParameter("name",name)
//		.list();
//		session.getTransaction().commit();
//		HibernateSessionFactory.closeSession();
//		return obj;
//	}
	//批量分析--根据医院和日期查询分析的人数
	@Override
	public int getCountBySearch(String phospital, Date ptime) {
		Session session = HibernateSessionFactory.getSession();
		session.beginTransaction();
		int total = 0;
		String strHql = "select count(*) from Patient as p  where p.";
		Object obj = null;
		strHql += "phospital";
		strHql += " =:a and p.";
		strHql += "ptime";
		strHql += " =:b";
		obj = session.createQuery(strHql)
		.setParameter("a",phospital)
		.setParameter("b",ptime)
		.iterate().next();
		if (obj != null)
			total = Integer.parseInt(obj.toString());
		session.getTransaction().commit();
		HibernateSessionFactory.closeSession();
		return total;
	}

	@Override
	public int getCountBySearch(String phospital, String ptime, String presult) {
		Session session = HibernateSessionFactory.getSession();
		session.beginTransaction();
		int total = 0;
		String strHql = "select count(*) from Patient as p  where p.";
		Object obj = null;
		strHql += "phospital";
		strHql += " =:a and p.";
		strHql += "ptime";
		strHql += " =:b and p.";
		strHql += "presult";
		strHql += " =:c";
		obj = session.createQuery(strHql)
		.setParameter("a",phospital)
		.setParameter("b",ptime)
		.setParameter("c",presult)
		.iterate().next();
		if (obj != null)
			total = Integer.parseInt(obj.toString());
		session.getTransaction().commit();
		HibernateSessionFactory.closeSession();
		return total;
	}
}
