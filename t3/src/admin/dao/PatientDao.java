package admin.dao;


import java.sql.Date;
import java.util.List;

import admin.bean.Patient;

import com.page.Pager;

public interface PatientDao {

	public void save(Patient p);//插入新用户
	public void delete(Patient p);
	public void update(Patient p);
	public Patient findPatientById(Integer id);
	public List<Patient> searchOne(String name,String phospital,String pgender,String num);
	public List<Patient> searchByhostpital(String hospital,Date date);
	//public List<Patient> searchByhospital(String hospital);//还实现 
	public List<Patient> queryAllPatient();
	public List<Patient> queryAllPatient(Pager pager);//根据分页范围查询
	public int getTotalRows();//获取数据库的总记录数

	
	public int getCountBySearch(int searchType,String searchValue);

	public List<Patient> queryBySearch(int searchType,String searchValue,Pager pager);
	
	public int getCountBySearch(String phospital, Date ptime);
	public int getCountBySearch(String phospital, String ptime,String presult);
}
