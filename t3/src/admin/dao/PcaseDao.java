package admin.dao;


import java.util.List;

import admin.bean.Patient;
import admin.bean.Pcase;

import com.page.Pager;

public interface PcaseDao {

	public void save(Pcase p);//插入新用户
	public void delete(Pcase p);
	public void update(Pcase p);
	public Pcase findPcaseById(Integer id);
	public List<Pcase> queryAllPcase();
	public List<Pcase> queryAllPcase(Pager pager);//根据分页范围查询
	public int getTotalRows();//获取数据库的总记录数

	public int getCountBySearch(int searchType,String searchValue);

	public List<Pcase> queryBySearch(int searchType,String searchValue,Pager pager);
}
