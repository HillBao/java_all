package admin.dao;


import java.util.List;

import admin.bean.Patient;
import admin.bean.Pcase;
import admin.bean.Test;

import com.bean.User;
import com.page.Pager;

public interface TestDao {

	public void save(Test t);//插入新用户
	public void delete(Test t);
	public void update(Test t);
	public Test findTestById(Integer id);
	public List<Test> queryAllTest();
	public List<Test> searchOne(String name);
	public List<Test> queryAllTest(Pager pager);//根据分页范围查询
	public int getTotalRows();//获取数据库的总记录数

	public int getCountBySearch(int searchType,String searchValue);

	public List<Test> queryBySearch(int searchType,String searchValue,Pager pager);
	 
}
