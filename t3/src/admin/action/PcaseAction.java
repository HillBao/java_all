package admin.action;

import java.util.List;
import javax.servlet.http.HttpServletRequest;
import org.apache.struts2.ServletActionContext;
import admin.bean.Patient;
import admin.bean.Pcase;
import admin.dao.PatientDao;
import admin.dao.PcaseDao;
import admin.dao.impl.PatientDaoImpl;
import admin.dao.impl.PcaseDaoImpl;

import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;
import com.page.Pager;
import com.page.PagerHelper;

public class PcaseAction extends ActionSupport {

	private Pcase pcase;
	private PcaseDao pcaseDao = new PcaseDaoImpl();
	private String validateCode;
	private Integer searchType;
	private String searchValue;

	public Integer getSearchType() {
		return searchType;
	}

	public void setSearchType(Integer searchType) {
		this.searchType = searchType;
	}

	public String getSearchValue() {
		return searchValue;
	}

	public void setSearchValue(String searchValue) {
		this.searchValue = searchValue;
	}

	public String getValidateCode() {
		return validateCode;
	}

	public void setValidateCode(String validateCode) {
		this.validateCode = validateCode;
	}


	public Pcase getPcase() {
		return pcase;
	}

	public void setPcase(Pcase pcase) {
		this.pcase = pcase;
	}



	public PcaseDao getPcaseDao() {
		return pcaseDao;
	}

	public void setPcaseDao(PcaseDao pcaseDao) {
		this.pcaseDao = pcaseDao;
	}

	// 查询action
	public String pcaseSearch() throws Exception {
		int totalRows = 0;// 记录查询总行数，待hibernate计算
		Pager pager = null; // 页面信息
		int pageSize = 10;// 每页记录数
		HttpServletRequest request = ServletActionContext.getRequest();
		totalRows = pcaseDao.getCountBySearch(searchType, searchValue); // 获取指定类型的记录个数
		pager = PagerHelper.getPager(request, totalRows, pageSize); // 初始化分页对象
		pager.setLinkUrl("pcaseSearch.action?"); // 设置跳转路径，也可以是？&
		request.setAttribute("pb", pager); // 将分页信息保存在Request对象pb中
		List<Pcase> patientList = pcaseDao.queryBySearch(searchType, searchValue
				.trim(), pager);// 根据分页范围内的list
		request.setAttribute("pcaseList", patientList);
		ActionContext ctx = ActionContext.getContext();
		ctx.getSession().put("url", "pcaseSearch.action?");// 把url的值放到session内，方便修改删除后再次跳转到该
		return SUCCESS; 
	 
	}

	public String listAllPcasePage() throws Exception {
		int totalRows = 0;// 记录总行数，待hibernate计算
		Pager pager = null; // 页面信息
		int pageSize = 10;// 每页记录数
		HttpServletRequest request = ServletActionContext.getRequest();
		totalRows = pcaseDao.getTotalRows(); // 获取数据库总行数
		pager = PagerHelper.getPager(request, totalRows, pageSize); // 初始化分页对象
		pager.setLinkUrl("listAllPcasePage.action?"); // 设置跳转路径，也可以是？&
		request.setAttribute("pb", pager); // 将分页信息保存在Request对象pb中
		List<Pcase> patientList = pcaseDao.queryAllPcase(pager);// 根据分页范围内的list
		request.setAttribute("pcaseList", patientList);
		return SUCCESS;
	}

	public String insertAction() throws Exception {
		this.pcaseDao.save(this.pcase);
		this.addActionError("注册成功，请登录");
		return SUCCESS;
	}

	// 批量删除
	public String delChecked() throws Exception {
		HttpServletRequest request = ServletActionContext.getRequest();
		String[] pids = request.getParameterValues("ids");
		for (String pid : pids) {
			Pcase p = this.pcaseDao.findPcaseById(Integer.parseInt(pid));
			this.pcaseDao.delete(p);
		}
		return listAllPcasePage();
	}

	// 根据传过来的uid取到User，删除User、
	public String deleteAction() throws Exception {
		HttpServletRequest request = ServletActionContext.getRequest();
		String pid = request.getParameter("pid");
		Pcase p = this.pcaseDao.findPcaseById(Integer.parseInt(pid));
		this.pcaseDao.delete(p);
		return listAllPcasePage();
	}

	// 根据传过来的uid取到User，打开修改页面
	public String preUpdateAction() throws Exception {
		HttpServletRequest request = ServletActionContext.getRequest();
		String pid = request.getParameter("pid");
		Pcase p = this.pcaseDao.findPcaseById(Integer.parseInt(pid));
		request.setAttribute("p", p);
		return SUCCESS;
	}

	// 根据传过来的uid取到User，打开修改页面
	public String updateAction() throws Exception {
		this.pcaseDao.update(this.pcase);
		return listAllPcasePage();
	}

}
