package admin.action;

import java.util.List;

import javax.jms.Session;
import javax.servlet.http.HttpServletRequest;
import org.apache.struts2.ServletActionContext;

import admin.bean.Patient;
import admin.bean.Pcase;
import admin.bean.Test;
import admin.dao.TestDao;
import admin.dao.impl.TestDaoImpl;

import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;
import com.page.Pager;
import com.page.PagerHelper;

public class TestAction extends ActionSupport {

	private Test test;
	private TestDao testDao = new TestDaoImpl();
	private String validateCode;
	private Integer searchType;
	private String searchValue;

	public Integer getSearchType() {
		return searchType;
	}

	public void setSearchType(Integer searchType) {
		this.searchType = searchType;
	}

	public String getSearchValue() {
		return searchValue;
	}

	public void setSearchValue(String searchValue) {
		this.searchValue = searchValue;
	}

	public String getValidateCode() {
		return validateCode;
	}

	public void setValidateCode(String validateCode) {
		this.validateCode = validateCode;
	}

	 
	public Test getTest() {
		return test;
	}

	public void setTest(Test test) {
		this.test = test;
	}


	public TestDao getTestDao() {
		return testDao;
	}

	public void setTestDao(TestDao testDao) {
		this.testDao = testDao;
	}

	// 查询action
	public String testSearch() throws Exception {
		int totalRows = 0;// 记录查询总行数，待hibernate计算
		Pager pager = null; // 页面信息
		int pageSize = 10;// 每页记录数
		HttpServletRequest request = ServletActionContext.getRequest();
		totalRows = testDao.getCountBySearch(searchType, searchValue); // 获取指定类型的记录个数
		pager = PagerHelper.getPager(request, totalRows, pageSize); // 初始化分页对象
		pager.setLinkUrl("testSearch.action?"); // 设置跳转路径，也可以是？&
		request.setAttribute("pb", pager); // 将分页信息保存在Request对象pb中
		List<Test> testList = testDao.queryBySearch(searchType, searchValue
				.trim(), pager);// 根据分页范围内的list
		request.setAttribute("testList", testList);
		ActionContext ctx = ActionContext.getContext();
		ctx.getSession().put("url", "testSearch.action?");// 把url的值放到session内，方便修改删除后再次跳转到该
		return SUCCESS; 
	}

	public String listAllTestPage() throws Exception {
		int totalRows = 0;// 记录总行数，待hibernate计算
		Pager pager = null; // 页面信息
		int pageSize = 10;// 每页记录数
		HttpServletRequest request = ServletActionContext.getRequest();
		totalRows = testDao.getTotalRows(); // 获取数据库总行数
		pager = PagerHelper.getPager(request, totalRows, pageSize); // 初始化分页对象
		pager.setLinkUrl("listAllTestPage.action?"); // 设置跳转路径，也可以是？&
		request.setAttribute("pb", pager); // 将分页信息保存在Request对象pb中
		List<Test> patientList = testDao.queryAllTest(pager);// 根据分页范围内的list
		request.setAttribute("testList", patientList);
		return SUCCESS;
	}

	public String insertAction() throws Exception {
		this.testDao.save(this.test);
		this.addActionError("注册成功，请登录");
		return SUCCESS;
	}

	// 批量删除
	public String delChecked() throws Exception {
		HttpServletRequest request = ServletActionContext.getRequest();
		String[] pids = request.getParameterValues("ids");
		for (String pid : pids) {
			Test p = this.testDao.findTestById(Integer.parseInt(pid));
			this.testDao.delete(p);
		}
		return listAllTestPage();
	}

	// 根据传过来的uid取到User，删除User、
	public String deleteAction() throws Exception {
		HttpServletRequest request = ServletActionContext.getRequest();
		String pid = request.getParameter("pid");
		Test p = this.testDao.findTestById(Integer.parseInt(pid));
		this.testDao.delete(p);
		return listAllTestPage();
	}

	// 根据传过来的tid取到Test，打开修改页面
	public String preUpdateAction() throws Exception {
		HttpServletRequest request = ServletActionContext.getRequest();
		String pid = request.getParameter("pid");
		Test p = this.testDao.findTestById(Integer.parseInt(pid));
		ActionContext.getContext().getSession().put("tname",
				p.getTname());
		request.setAttribute("p", p);
		return SUCCESS;
	}
	
	// 根据传过来的tid取到Test，打开修改页面
	public String preUpdateAction_test() throws Exception {
		HttpServletRequest request = ServletActionContext.getRequest();
		String pid = request.getParameter("pid");
		Test p = this.testDao.findTestById(Integer.parseInt(pid));
		request.setAttribute("testList", p);
		return SUCCESS;
	}


	// 根据传过来的uid取到User，打开修改页面
	public String updateAction() throws Exception {
		this.testDao.update(this.test);
		return listAllTestPage();
	}

	public String searchOne() throws Exception {
		HttpServletRequest request = ServletActionContext.getRequest();
		String na=(String)ActionContext.getContext().getSession().get("tname");
		List<Test> testList = testDao.searchOne(na);
		if(testList.size()==0){
			this.addActionMessage("没有查询到记录！");
		} 
		else{
		 request.setAttribute("testList", testList);
		}
		return SUCCESS;
	}
	
	public String testsearchOne() throws Exception {
		HttpServletRequest request = ServletActionContext.getRequest();
		List<Test> testList = testDao.searchOne(this.test.getTname());
		if(testList.size()==0){
			this.addActionMessage("该用户没有测试结果！");
		} 
		else{
		 request.setAttribute("testList", testList);
		}
		return SUCCESS;
	}
	public String turn() throws Exception{
		return SUCCESS;
	}
	
	
	public String count_all_test(){
		HttpServletRequest request = ServletActionContext.getRequest();
		int a=this.testDao.getTotalRows();
		request.setAttribute("a", a);
		return SUCCESS;
	}
}
