package admin.action;

import java.util.List;
import javax.servlet.http.HttpServletRequest;
import org.apache.struts2.ServletActionContext;
import admin.bean.Patient;
import admin.bean.Test;
import admin.dao.PatientDao;
import admin.dao.impl.PatientDaoImpl;

import com.bean.User;
import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;
import com.page.Pager;
import com.page.PagerHelper;

public class PatientAction extends ActionSupport {

	private Patient patient;
	private PatientDao patientDao = new PatientDaoImpl();
	private String validateCode;
	private Integer searchType;
	private String searchValue;

	public Integer getSearchType() {
		return searchType;
	}

	public void setSearchType(Integer searchType) {
		this.searchType = searchType;
	}

	public String getSearchValue() {
		return searchValue;
	}

	public void setSearchValue(String searchValue) {
		this.searchValue = searchValue;
	}

	public String getValidateCode() {
		return validateCode;
	}

	public void setValidateCode(String validateCode) {
		this.validateCode = validateCode;
	}


	public Patient getPatient() {
		return patient;
	}

	public void setPatient(Patient patient) {
		this.patient = patient;
	}

	public PatientDao getPatientDao() {
		return patientDao;
	}

	public void setPatientDao(PatientDao patientDao) {
		this.patientDao = patientDao;
	}

	// 查询action
	public String patientSearch() throws Exception {
		int totalRows = 0;// 记录查询总行数，待hibernate计算
		Pager pager = null; // 页面信息
		int pageSize = 10;// 每页记录数
		HttpServletRequest request = ServletActionContext.getRequest();
		totalRows = patientDao.getCountBySearch(searchType, searchValue); // 获取指定类型的记录个数
		pager = PagerHelper.getPager(request, totalRows, pageSize); // 初始化分页对象
		pager.setLinkUrl("patientSearch.action?"); // 设置跳转路径，也可以是？&
		request.setAttribute("pb", pager); // 将分页信息保存在Request对象pb中
		List<Patient> patientList = patientDao.queryBySearch(searchType, searchValue
				.trim(), pager);// 根据分页范围内的list
		request.setAttribute("patientList", patientList);
		ActionContext ctx = ActionContext.getContext();
		ctx.getSession().put("url", "patientSearch.action?");// 把url的值放到session内，方便修改删除后再次跳转到该
		return SUCCESS; 
 
	}


	public String listAllPatientPage() throws Exception {
		int totalRows = 0;// 记录总行数，待hibernate计算
		Pager pager = null; // 页面信息
		int pageSize = 10;// 每页记录数
		HttpServletRequest request = ServletActionContext.getRequest();
		totalRows = patientDao.getTotalRows(); // 获取数据库总行数
		pager = PagerHelper.getPager(request, totalRows, pageSize); // 初始化分页对象
		pager.setLinkUrl("listAllPatientPage.action?"); // 设置跳转路径，也可以是？&
		request.setAttribute("pb", pager); // 将分页信息保存在Request对象pb中
		List<Patient> patientList = patientDao.queryAllPatient(pager);// 根据分页范围内的list
		request.setAttribute("patientList", patientList);
		return SUCCESS;
	}

	public String insertAction() throws Exception {
		this.patientDao.save(this.patient);
		this.addActionError("注册成功，请登录");
		return SUCCESS;
	}

	// 批量删除
	public String delChecked() throws Exception {
		HttpServletRequest request = ServletActionContext.getRequest();
		String[] pids = request.getParameterValues("ids");
		for (String pid : pids) {
			Patient p = this.patientDao.findPatientById(Integer.parseInt(pid));
			this.patientDao.delete(p);
		}
		return listAllPatientPage();
	}

	// 根据传过来的uid取到User，删除User、
	public String deleteAction() throws Exception {
		HttpServletRequest request = ServletActionContext.getRequest();
		String pid = request.getParameter("pid");
		Patient p = this.patientDao.findPatientById(Integer.parseInt(pid));
		this.patientDao.delete(p);
		// List<User> userList = this.userDao.queryAllUser();
		// request.setAttribute("userList", userList);
		return listAllPatientPage();
	}

	// 根据传过来的uid取到User，打开修改页面
	public String preUpdateAction() throws Exception {
		HttpServletRequest request = ServletActionContext.getRequest();
		String pid = request.getParameter("pid");
		Patient p = this.patientDao.findPatientById(Integer.parseInt(pid));
		request.setAttribute("p", p);
		ActionContext.getContext().getSession().put("idd",
				pid);
		return SUCCESS;
	}

	// 根据传过来的uid取到User，打开修改页面
	public String updateAction() throws Exception {
		this.patientDao.update(this.patient);
		return listAllPatientPage();
	}

	public String searchOne() throws Exception {
		HttpServletRequest request = ServletActionContext.getRequest();
		List<Patient> patientList = patientDao.searchOne(this.patient.getPname(),this.patient.getPhospital(),this.patient.getPgender(),this.patient.getPnumber());
		if(patientList.size()==0){
			this.addActionMessage("没有查询到记录！");
		} 
		else{
//			ActionContext.getContext().getSession().put("idd",
//					this.patient.getPid());
//			System.out.println(this.patient.getPid());
		 request.setAttribute("patientList", patientList);
		}
		return SUCCESS;
	}
	
  //
	public String searchResult() throws Exception {
		HttpServletRequest request = ServletActionContext.getRequest();
		String pid=(String)ActionContext.getContext().getSession().get("idd");
		Patient p = this.patientDao.findPatientById(Integer.parseInt(pid));
		request.setAttribute("p", p);
		return SUCCESS;
	}
	

	//根据医院名称和就诊日期查询所有患者信息
	public String searchByhospital() throws Exception {
		HttpServletRequest request = ServletActionContext.getRequest();
		List<Patient> patientList = patientDao.searchByhostpital(this.patient.getPhospital(),this.patient.getPtime());
		 request.setAttribute("patientList", patientList);
		return SUCCESS;
	}
	
	
	//根据医院名称和就诊日期查询所有患者个数
	public String searchCount() throws Exception {
		HttpServletRequest request = ServletActionContext.getRequest();
		int a=this.patientDao.getCountBySearch(this.patient.getPhospital(),this.patient.getPtime());
		ActionContext.getContext().getSession().put("b",
				a);
		return SUCCESS;
	}

}
