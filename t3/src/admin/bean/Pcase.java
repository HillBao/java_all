package admin.bean;

import javax.persistence.*;

import org.hibernate.annotations.GenericGenerator;

@Entity
@Table(name = "pcase")
public class Pcase {

	@Id
	@Column(name = "id")
	@GenericGenerator(name = "myGen",strategy = "native")
	@GeneratedValue(generator = "myGen")
	private Integer cid;
	@Column(name = "name",length = 20)
	private String pname;  
	@Column(name = "hospital")
	private String phospital;  
	@Column(name = "result")
	private String cresult;  
	@Column(name = "tel")
	private String ptel; 
	@Column(name = "c1")
	private String c1;
	@Column(name = "c2")
	private String c2;
	@Column(name = "c3")
	private String c3;
	@Column(name = "c4")
	private String c4;
	@Column(name = "c5")
	private String c5;
	public Pcase() {
		super();
	}
	public Integer getCid() {
		return cid;
	}
	public void setCid(Integer cid) {
		this.cid = cid;
	}
	public String getPname() {
		return pname;
	}
	public void setPname(String pname) {
		this.pname = pname;
	}
	public String getPhospital() {
		return phospital;
	}
	public void setPhospital(String phospital) {
		this.phospital = phospital;
	}
	public String getCresult() {
		return cresult;
	}
	public void setCresult(String cresult) {
		this.cresult = cresult;
	}
	public String getPtel() {
		return ptel;
	}
	public void setPtel(String ptel) {
		this.ptel = ptel;
	}
	public String getC1() {
		return c1;
	}
	public void setC1(String c1) {
		this.c1 = c1;
	}
	public String getC2() {
		return c2;
	}
	public void setC2(String c2) {
		this.c2 = c2;
	}
	public String getC3() {
		return c3;
	}
	public void setC3(String c3) {
		this.c3 = c3;
	}
	public String getC4() {
		return c4;
	}
	public void setC4(String c4) {
		this.c4 = c4;
	}
	public String getC5() {
		return c5;
	}
	public void setC5(String c5) {
		this.c5 = c5;
	} 
 
	 
}
