package admin.bean;

import javax.persistence.*;

import org.hibernate.annotations.GenericGenerator;

@Entity
@Table(name = "test") 
public class Test {

	@Id
	@Column(name = "id")
	@GenericGenerator(name = "myGen",strategy = "native")
	@GeneratedValue(generator = "myGen")
	private Integer tid;
	@Column(name = "name",length = 20)
	private String tname; 
	@Column(name = "cresult")
	private String cresult; 
	@Column(name = "presult")
	private String presult;  
	@Column(name = "c1")
	private String c1;
	@Column(name = "c2")
	private String c2;
	@Column(name = "c3")
	private String c3;
	@Column(name = "c4")
	private String c4;
	@Column(name = "c5")
	private String c5;
	public Test() {
		super();
	}
	public Integer getTid() {
		return tid;
	}
	public void setTid(Integer tid) {
		this.tid = tid;
	}
	
	public String getTname() {
		return tname;
	}
	public void setTname(String tname) {
		this.tname = tname;
	}
	public String getCresult() {
		return cresult;
	}
	public void setCresult(String cresult) {
		this.cresult = cresult;
	}
	public String getPresult() {
		return presult;
	}
	public void setPresult(String presult) {
		this.presult = presult;
	}
	public String getC1() {
		return c1;
	}
	public void setC1(String c1) {
		this.c1 = c1;
	}
	public String getC2() {
		return c2;
	}
	public void setC2(String c2) {
		this.c2 = c2;
	}
	public String getC3() {
		return c3;
	}
	public void setC3(String c3) {
		this.c3 = c3;
	}
	public String getC4() {
		return c4;
	}
	public void setC4(String c4) {
		this.c4 = c4;
	}
	public String getC5() {
		return c5;
	}
	public void setC5(String c5) {
		this.c5 = c5;
	}
	 
	 
}
