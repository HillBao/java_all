package admin.bean;

import java.sql.Date;

import javax.persistence.*;

import org.hibernate.annotations.GenericGenerator;

@Entity
@Table(name = "patients")
//@Cache(usage = CacheConcurrencyStrategy.READ_ONLY)
public class Patient {

	@Id
	@Column(name = "id")
	@GenericGenerator(name = "myGen",strategy = "native")
	@GeneratedValue(generator = "myGen")
	private Integer pid;
	@Column(name = "name",length = 20)
	private String pname;
	@Column(name = "gender")
	private String pgender;
	@Column(name = "birthday")
	private Date pbirthday;
	@Column(name = "hospital")
	private String phospital;
	@Column(name = "number")
	private String pnumber;
	@Column(name = "time")
	private Date ptime;
 
	@Column(name = "email")
	private String pemail;
	@Column(name = "c1")
	private String c1;
	@Column(name = "c2")
	private String c2;
	@Column(name = "c3")
	private String c3;
	@Column(name = "c4")
	private String c4;
	@Column(name = "c5")
	private String c5; 
	@Column(name = "presult")//�жϽ��
	private String presult; 
	
	
	
	
	public String getPemail() {
		return pemail;
	}




	public void setPemail(String pemail) {
		this.pemail = pemail;
	}




	public Patient() {
		super();
	}




	public String getPgender() {
		return pgender;
	}




	public void setPgender(String pgender) {
		this.pgender = pgender;
	}




	public Integer getPid() {
		return pid;
	}




	public void setPid(Integer pid) {
		this.pid = pid;
	}




	public String getPname() {
		return pname;
	}




	public void setPname(String pname) {
		this.pname = pname;
	}

	

	public Date getPbirthday() {
		return pbirthday;
	}




	public void setPbirthday(Date pbirthday) {
		this.pbirthday = pbirthday;
	}




	public String getPhospital() {
		return phospital;
	}




	public void setPhospital(String phospital) {
		this.phospital = phospital;
	}




	public String getPnumber() {
		return pnumber;
	}




	public void setPnumber(String pnumber) {
		this.pnumber = pnumber;
	}



 



	public Date getPtime() {
		return ptime;
	}




	public void setPtime(Date ptime) {
		this.ptime = ptime;
	}




	public String getC1() {
		return c1;
	}




	public void setC1(String c1) {
		this.c1 = c1;
	}


	public String getC2() {
		return c2;
	}




	public void setC2(String c2) {
		this.c2 = c2;
	}




	public String getC3() {
		return c3;
	}




	public void setC3(String c3) {
		this.c3 = c3;
	}




	public String getC4() {
		return c4;
	}




	public void setC4(String c4) {
		this.c4 = c4;
	}




	public String getC5() {
		return c5;
	}




	public void setC5(String c5) {
		this.c5 = c5;
	}




	public String getPresult() {
		return presult;
	}




	public void setPresult(String presult) {
		this.presult = presult;
	}
 
	
}
